<?php

namespace App\Fine\Product;

use Illuminate\Session\Store;
use Mockery\Exception;

class ProductCUD
{
    private $productPageModifier = null;

    private $imageFolder = "compounds/images/";

    private $descFolder = "compounds/";

    public function __construct(ProductPageModifier $ppm)
    {
        $this->productPageModifier = $ppm;
    }

    public function CreateProduct($name, $cas, $purity)
    {
        if(isset(\App\Product::where('name', $name)->first()->id))
        {
            throw new Exception("Product already exists");
        }

        $id = \App\Product::create([
            'name' => $name,
            'CAS' => $cas,
            'Purity' => $purity,
        ])->id;

        return \App\Product::find($id);
    }

    public function UpdateProduct($product, $name, $cas, $purity)
    {
        $product->name = $name;
        $product->CAS = $cas;
        $product->Purity = $purity;

        $product->save();

        return $this;
    }

    public function AddClasses($product, $compClasses = null)
    {
        if($product->classes())
        {
            $product->classes()->detach();
        }

        $classes = \App\CompClass::all();

        foreach($classes as $compClass)
        {
            if(isset($compClasses[$compClass->id]))
            {
                $product->classes()->attach($compClass->id);
            }
        }

        return $this;
    }

    public function AddPrices($product, $prices = null)
    {
        if($product->prices())
        {
            $product->prices()->delete();
        }

        if($prices == null)
        {
            return;
        }

        for($i = 0; $i < count($prices); $i++)
        {
            if($prices[$i] != null)
            {
                $product->prices()->create(['price' => $prices[$i]]);
            }
        }

        return $this;
    }

    public function AddFiles($image = null, $desc = null, $id)
    {
        if($image !== null && $desc !== null)
        {
            $image->move($this->imageFolder, $id . "." . $image->getClientOriginalExtension());

            $desc->move($this->descFolder, $id . '.htm');
            $this->productPageModifier->Modify($this->descFolder . $id . '.htm', $id . '.' . $desc->getClientOriginalExtension());
        }

        return $this;
    }

    public function Delete($product)
    {
        $product->delete();
    }

}
