<?php

namespace App\Fine\Product;

class ProductPageModifier
{

    public function Modify($fileName, $imgName)
    {
        $fileLines = explode("\n", file_get_contents($fileName));

        $imgLineNr = $this->FindImgLine($fileLines);

        $lineStart = explode('src="', $fileLines[$imgLineNr])[0];

        $lineEnd = explode('">', $fileLines[$imgLineNr])[1];

        $fileLines[$imgLineNr] = $lineStart . 'src="' . $imgName . '">' . $lineEnd;

        $this->Save($fileName, $fileLines);
    }

    private function FindImgLine($lines)
    {
        for($i = 0; $i < count($lines); $i++)
        {
            $srcPos = strpos($lines[$i], 'src="');

            if($srcPos !== false)
            {
                return $i;
            }
        }
    }

    private function Save($fileName, $lines)
    {
        $content = '';

        foreach($lines as $line)
        {
            $content = $content . $line . "\n";
        }

        file_put_contents($fileName, $content);
    }
}
