<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fine\Product\ProductCUD;
use Mockery\Exception;

class ProductsController extends Controller
{
    private $productsPerPage = 15;

    private $productCUD = null;

    public function __construct(ProductCUD $pcud)
    {
        $this->productCUD = $pcud;

        $this->middleware('auth')->except(['index', 'showClass', 'showSearch', 'show']);
    }


    public function index()
    {
        $products = \App\Product::orderBy('name', 'asc')->paginate($this->productsPerPage);

        $compClasses = \App\CompClass::orderBy('name', 'asc')->get();

        return view('products', compact('products', 'compClasses'));
    }


    public function create()
    {
        $classes = \App\CompClass::all();

        return view('admin.uploadproduct', compact('classes'));
    }


    public function store(Request $request)
    {
        $newCompound = null;

        try {
            $newCompound = $this->productCUD->CreateProduct($request->name, $request->cas, $request->purity);
        }
        catch(Exception $e)
        {
            $request->session()->flash('session_msg', $e->getMessage());
            return redirect('/products/list');
        }

        $this->productCUD->AddClasses($newCompound, $request->compclasses)
                         ->AddPrices($newCompound, $request->prices)
                         ->AddFiles($request->file('image'), $request->file('productpage'), $newCompound->id);

        return redirect('/products/' . $newCompound->id);
    }

    public function showClass($name)
    {
        $products = \App\CompClass::where('name', $name)->first()->products()->orderBy('name', 'asc')->paginate($this->productsPerPage);

        $compClasses = \App\CompClass::orderBy('name', 'asc')->get();

        return view('products', compact('products', 'compClasses'));
    }


    // Paginated search results
    public function showSearch(Request $request)
    {
        $query = $request->input('query');

        $products = null;

        if(preg_match("/[0-9]{2,7}-[0-9]{2,5}/", $query)){
            $products = \App\Product::where('CAS', 'like', '%' . $query . '%')->orderBy('name', 'asc')->paginate($this->productsPerPage);
        }else{
            $products = \App\Product::where('name', 'like', '%' . $query . '%')->orderBy('name', 'asc')->paginate($this->productsPerPage);
        }

        $compClasses = \App\CompClass::orderBy('name', 'asc')->get();

        return view('products', compact('products', 'compClasses'));
    }


    public function show(\App\Product $product)
    {
        $compClasses = $product->classes()->orderBy('name', 'asc')->get();

        return view('product', compact('product', 'compClasses'));
    }


    public function edit(\App\Product $product){
        $prices = $product->prices()->get()->toArray();

        $classes = \App\CompClass::all();

        return view('admin.uploadproduct', compact('product', 'classes', 'prices'));
    }


    public function update(Request $request, \App\Product $product)
    {
        $this->productCUD->UpdateProduct($product, $request->name, $request->cas, $request->purity)
                         ->AddClasses($product, $request->compclasses)
                         ->AddPrices($product, $request->prices);

        return redirect('/products/' . $product->id . '/edit');
    }


    public function destroy(\App\Product $product)
    {
        $this->productCUD->delete($product);

        return redirect('/products/list');
    }


    public function list()
    {
        $items = \App\Product::orderBy('name', 'asc')->get();

        $modelname = 'products';

        return view('admin.list', compact('items', 'modelname'));
    }
}
