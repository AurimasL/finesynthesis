<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        $paragraphs = \App\Info::where('type', 'about')->get();

        return view('/about', compact('paragraphs'));
    }

    public function edit()
    {
        $aboutpars = \App\Info::where('type', 'about')->get();

        return view('admin.updateabout', compact('aboutpars'));
    }

    public function update(Request $request)
    {
        \App\Info::where('type', 'about')->delete();

        for($i = 0; $i < count($request->parnames); $i++)
        {
            if($request->parnames[$i] != null)
            {
                $paragraph = new \App\Info;

                $paragraph->type = 'about';
                $paragraph->name = $request->parnames[$i];
                $paragraph->body = $request->parbodies[$i];

                $paragraph->save();
            }
        }

        return redirect('/about');
    }
}
