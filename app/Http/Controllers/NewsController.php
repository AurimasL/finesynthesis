<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Carbon\Carbon;

class NewsController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index');
    }


    public function index()
    {
        $posts = \App\Post::latest()->get();

        return view('news', compact('posts'));
    }


    public function create()
    {
        return view('admin.postnews');
    }


    public function store(Request $request)
    {
        $post = new \App\Post;

        $post->title = $request->title;
        $post->body = TextToHTML($request->body);
        $post->image = 'none';

        if($request->file('image') !== null){
            $file = $request->file('image');
            $file->move('images/news', $file->getClientOriginalName());
            $post->image = $file->getClientOriginalName();
        }

        $post->save();

        return redirect('/news');
    }


    public function edit(\App\Post $news)
    {
        return view('admin.postnews', compact('news'));
    }


    public function update(Request $request, \App\Post $news)
    {
        $news->title = $request->title;
        $news->body = TextToHTML($request->body);
        $news->save();

        return redirect('/news');
    }


    public function destroy(\App\Post $news)
    {
        if($news->image != 'none'){
            //dd('images/news/' . $post->image);
            File::delete('images/news/' . $news->image);
        }

        $news->delete();

        return redirect('/news');
    }

    public function list()
    {
        $items = \App\Post::latest()->get(['title AS name', 'id']);

        $modelname = 'news';

        return view('admin.list', compact('items', 'modelname'));
    }
}
