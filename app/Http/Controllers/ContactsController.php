<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;

class ContactsController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->only(['edit', 'update']);
    }

    public function index()
    {
        $contacts = \App\Info::where('type', 'contact')->get();

        return view('contacts', compact('contacts'));
    }

    public function edit()
    {
        $contacts = \App\Info::where('type', 'contact')->get();

        return view('admin.updatecontacts', compact('contacts'));
    }

    public function update(Request $request)
    {
        \App\Info::where('type', 'contact')->delete();

        for($i = 0; $i < count($request->contactnames); $i++)
        {
            if($request->contactnames[$i] != null)
            {
                $contact = new \App\Info;

                $contact->type = 'contact';
                $contact->name = $request->contactnames[$i];
                $contact->body = $request->contactvalues[$i];

                $contact->save();
            }
        }

        return redirect('/contacts');
    }

    public function sendMail(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ], ['email.email' => 'Entered E-mail address is not valid']);


        if ($validator->fails()) {
            return redirect('/contacts');
        }

        Mail::raw($request->message, function ($message) use ($request) {
            $message->from($request->email, 'Customer')->subject('Inquiry: ' . $request->subject);

            $message->to(env('MAIL_USERNAME'));
        });

        Mail::raw($request->message, function ($message) use ($request) {
            $message->from(env('MAIL_USERNAME'), 'Fine Synthesis')->subject('[Copy] Inquiry: ' . $request->subject);

            $message->to($request->email);
        });

        $request->session()->flash('session_msg', "Inquiry was sent to administrators and your inbox: " . $request->email .
                                   ". We will contact you shortly.");

        return redirect('/contacts');
    }
}
