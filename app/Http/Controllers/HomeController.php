<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $slidetexts = \App\Info::where('type', 'slidetext')->get();

        return view('home', compact('slidetexts'));
    }
}
