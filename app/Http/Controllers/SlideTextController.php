<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SlideTextController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $slidetexts = \App\Info::where('type', 'slidetext')->get();

        return view('admin.updateslidetext', compact('slidetexts'));
    }

    public function update(Request $request)
    {
        \App\Info::where('type', 'slidetext')->delete();

        for($i = 0; $i < count($request->slidetexts); $i++)
        {
            if($request->slidetexts[$i] != null)
            {
                $slidetexts = new \App\Info;

                $slidetexts->type = 'slidetext';
                $slidetexts->name = 'slide' . ($i + 1);
                $slidetexts->body = $request->slidetexts[$i];

                $slidetexts->save();
            }
        }

        return redirect('/');
    }
}
