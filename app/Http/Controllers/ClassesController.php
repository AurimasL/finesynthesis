<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClassesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'links']);
    }


    public function index()
    {
        $compclasses = \App\CompClass::orderBy('name', 'asc')->get();

        return view('classes', compact('compclasses'));
    }


    public function create(){
        return view('admin.compoundclasses');
    }


    public function store(Request $request)
    {
        $newClass = new \App\CompClass;
        $newClass->name = $request->name;
        $newClass->save();

        return redirect('/compclasses/list');
    }


    public function edit(\App\CompClass $compclass)
    {
        return view('admin.compoundclasses', compact('compclass'));
    }


    public function update(Request $request, \App\CompClass $compclass)
    {
        $compclass->name = $request->name;
        $compclass->save();

        return redirect('/compclasses/list');
    }


    public function destroy(Request $request, \App\CompClass $compclass)
    {
        if($compclass->products()->count() == 0)
        {
            $compclass->delete();
        }
        else {
            $request->session()->flash('session_msg', "This class has products associated with it and can't be deleted");
        }

        return redirect('/compclasses/list');
    }


    public function list()
    {
        $items = \App\CompClass::orderBy('name', 'asc')->get();

        $modelname = 'compclasses';

        return view('admin.list', compact('items', 'modelname'));
    }


    public function links()
    {
        $compclasses = \App\CompClass::orderBy('name', 'asc')->get();

        return view('classes', compact('compclasses'));
    }
}
