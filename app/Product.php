<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = ['name', 'CAS', 'Purity'];

    
    public function classes()
    {
        return $this->belongsToMany('App\CompClass');
    }

    
    public function prices()
    {
        return $this->hasMany('App\Price');
    }
    

    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($product) 
        {
            $product->prices()->delete();  // Delete prices

            DB::table('comp_class_product')->where('product_id', $product->id)->delete(); // Delete connections with classes
        });
    }
}
