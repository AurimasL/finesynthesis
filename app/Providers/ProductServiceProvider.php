<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Fine\Product;

class ProductServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ProductPageModifier::class, function($app){
            return new ProductPageModifier();
        });

        $this->app->singleton(ProductCUD::class, function($app){
            return new ProductCUD();
        });
    }
}
