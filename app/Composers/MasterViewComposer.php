<?php

namespace App\Composers;

class MasterViewComposer {

    public function compose($view){
        $contacts = \App\Info::where('type', 'contact')->limit(2)->get();

        $compClasses = \App\CompClass::orderBy('name', 'asc')->get();

        $view->with('contacts', $contacts)->with('compClasses', $compClasses);
    }
}
