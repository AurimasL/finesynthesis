<?php

return [
    'message' => 'We use cookies to ensure you get the best user experience from the Fine Synthesis website.
                  If you continue without changing your settings, we will assume that you are happy to receive all cookies on this website.',
    'agree' => 'Allow cookies',
];
