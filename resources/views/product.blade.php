@extends('layouts.master')

@section('main_content')

<style>
    .product_frame {
        height: 600px;
        width: 100%;
        border: none;
    }
</style>

<div class="container">
    <div class="row">
        <div class="page-header">
            <h1>{{ $product->name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <iframe class="product_frame" src="/compounds/{{ $product->id }}.htm"></iframe>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default products_sidebar">
                <div class="panel-heading">Browse by class</div>
                <div class="list-group">
                    @foreach($compClasses as $compClass)
                        <a href="/products/class/{{ $compClass->name }}" class="list-group-item">{{ $compClass->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('includes')

    <link href="/css/products.css" rel="stylesheet" type="text/css">

@endsection
