@extends('layouts.master')

@section('main_content')

<style>
    .class_link {
        font-size: 14pt;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="page-header">
                <h1>Classes Of Products</h1>
            </div>

            <table class="table table-default">
                @foreach($compclasses as $compclass)
                    <tr><td><a class="class_link" href="/products/class/{{ $compclass->name }}">{{ $compclass->name }}</a></td></tr>
                @endforeach
            </table>


        </div>
    </div>
</div>

@endsection

@section('includes')



@endsection
