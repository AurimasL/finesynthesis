@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="page-header">
            <h1>Products</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
                @if(count($products))
                    <div class="custom_table">
                        <div class="custom_table_header">
                            <div class="custom_table_left">
                                Compound, Purity, CAS
                            </div>
                            <div class="custom_table_center">
                                Structure
                            </div>
                            <div class="custom_table_right">
                                Pricing
                            </div>
                        </div>

                        @foreach($products as $product)
                            <div class="custom_table_row">
                                <div class="custom_table_rowwrap">
                                    <div class="custom_table_left">
                                        <a href="/products/{{ $product->id }}">{{ $product->name }}</a>, {{ $product->purity }}<br/>{{ $product->CAS }}
                                    </div>
                                    <div class="custom_table_center">
                                        <img class="products_table_image" src="/compounds/images/{{ $product->id }}.png" alt="Structure not available">
                                    </div>
                                    <div class="custom_table_right">
                                        <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="false" data-toggle="collapse" data-target="#product{{ $product->id }}"></span>
                                    </div>
                                </div>
                                <div id="product{{ $product->id }}" class="collapse">
                                    <div class="custom_table_additional">
                                        <hr class="custom_table_hr"/>
                                        <b>Prices:</b><br/>
                                        @foreach($product->prices()->get() as $price)
                                            {{ $price->price }}<br/>
                                        @endforeach
                                        <a href="/contacts">inquire</a>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="no_results">No results found</div>
                @endif
            <!-- </div> -->
        </div>

        <div class="col-md-3">
            <div class="panel panel-default products_sidebar">
                <div class="panel-heading">Browse by class</div>
                <div class="list-group">
                    @foreach($compClasses as $compClass)
                        <a href="/products/class/{{ $compClass->name }}" class="list-group-item">{{ $compClass->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    {{ $products->appends(Request::only('query'))->links() }}
</div>

@endsection

@section('includes')

    <link href="/css/products.css" rel="stylesheet" type="text/css">
    <script src="/js/products.js"></script>

@endsection
