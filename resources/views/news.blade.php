@extends('layouts.master')

@section('main_content')

<style>
    .newsimg{
        max-width: 300px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @foreach($posts as $post)
                <div class="news_post">
                    <h2>{{ $post->title }}</h2>
                    <div class="date">{{ $post->created_at->toFormattedDateString() }}</div>
                    <p>{!! $post->body !!}</p>
                    <img class="newsimg" src="{{ $post->image == 'none' ? '' : '/images/news/' . $post->image }}"></img>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('includes')

    <link href="/css/news.css" rel="stylesheet" type="text/css">

@endsection
