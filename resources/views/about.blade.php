@extends('layouts.master')

@section('main_content')

<style>
    p {
        text-indent: 50px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="page-header">
                <h1>About Fine Synthesis</h1>
            </div>

            @foreach($paragraphs as $paragraph)
                @if($paragraph->name != 'none')
                    <br/>
                    <h3>{{ $paragraph->name }}</h3>
                    <br/>
                @endif
                <p>{{ $paragraph->body }}</p>

                <br/>
            @endforeach

        </div>

    </div>
</div>

@endsection

@section('includes')



@endsection
