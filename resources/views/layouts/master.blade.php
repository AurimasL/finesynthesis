<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Fine Synthesis</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/css/bs_override.css" rel="stylesheet" type="text/css">
        <link href="/css/styles.css" rel="stylesheet" type="text/css">
        <link href="/css/footer.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body>
        @include('cookieConsent::index')

        @include('layouts.nav')

        @include('layouts.message')

        @yield('main_content')

        @include('layouts.footer')

        <script src="/js/jquery.min.js" type="text/javascript"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>

        @yield('includes')
    </body>
</html>
