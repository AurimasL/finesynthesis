<li class="dropdown">
    <a href="#" class="dropdown-toggle fine_nav_btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Admin<span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li><a href="/products/list">Products</a></li>
        <li><a href="/compclasses/list">Compound classes</a></li>
        <li><a href="/news/list">News</a></li>
        <li><a href="/updatecontacts">Contacts</a></li>
        <li><a href="/updateabout">About</a></li>
        <li><a href="/updateslidetexts">Slide text</a></li>
        <li>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Log out
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</li>
