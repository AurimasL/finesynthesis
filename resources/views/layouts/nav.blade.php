<div class="navbar navbar-default fine_nav_top">
    <div class="container">

            <a class="navbar-brand" id="navBrandImgCont" href="/">
                <img alt="Brand" id="navBrandImg" src="/images/smallr.png">
            </a>
    </div>
</div>
<div class="navbar navbar-inverse fine_nav_bottom">
    <div class="container">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle fine_nav_btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        Products<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu product-index">
                        <li><a href="/products">Browse by name</a></li>
                        <!-- <li><a href="/classes">Browse by class</a></li> -->
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Browse by class</li>

                        <ul class="bs_drop_list">
                            @foreach($compClasses as $compClass)
                                @if($loop->index - $loop->remaining == 1 || $loop->index - $loop->remaining == 2)
                                    </ul>
                                    <ul class="bs_drop_list">
                                @endif

                                <li><a href="/products/class/{{ $compClass->name }}">{{ $compClass->name }}</a></li>

                            @endforeach
                        </ul>

                    </ul>
                </li>
                <li><a href="/news" class="fine_nav_btn">News</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                @if(Auth::check())
                    @include('layouts.adminnav')
                @endif

                <li><a href="/about" class="fine_nav_btn">About us</a></li>
                <li><a href="/contacts" class="fine_nav_btn">Contacts</a></li>
            </ul>
        </div>
    </div>
</div>
