<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-md-offset-3 ">
                <div class="fine_footer_section">
                    <h4>Products</h4>
                    <hr class="fine_footer_hr"/>
                    <ul class="fine_list">
                        <li><a href="/products">Browse by name</a></li>
                        <li><a href="/compclasses/links">Browse by class</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="fine_footer_section">
                    <h4>Contacts</h4>
                    <hr class="fine_footer_hr"/>
                    <ul class="fine_list">
                        @foreach($contacts as $contact)
                            <li>{{ $contact->name }}: {{ $contact->body }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                Fine Synthesis 2017&copy
            </div>
        </div>
    </div>
</div>
