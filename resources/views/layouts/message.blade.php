@if (Session::has('session_msg'))
    <style>
        .fine_alert{
            position: fixed;
            z-index: 100;
            width: 500px;
            left: 50%;
            margin-left: -250px;
            top: 200px;
            height: 100px;
            color: black;
        }
    </style>

    <div class="alert alert-danger alert-dismissible fine_alert" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('session_msg') }}
    </div>
@endif
