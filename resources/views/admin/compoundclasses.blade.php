@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <br/>
            <br/>
            <h1>Class</h1>
            <br/>
            <br/>

            <br/>
            <br/>


            @if(isset($compclass))
                <form class="form-horizontal" role="form" method="POST" action="/compclasses/{{ $compclass->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $compclass->name }}"
                                placeholder="Class name" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <b>Number of compounds:</b> {{ $compclass->products()->count() }}<br/><br/>
                    </div>

                    <div class="form-group">
                        <button type="submit">
                            Save
                        </button>
                    </div>
                </form>

                @if( $compclass->products()->count() == 0 )
                    <form class="form-horizontal" role="form" method="POST" action="/compclasses/{{ $compclass->id }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <button type="submit">
                                Delete
                            </button>
                        </div>
                    </form>
                @endif
            @else
                <form class="form-horizontal" role="form" method="POST" action="/compclasses">
                    {{ csrf_field() }}

                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="name" type="text" class="form-control" name="name" placeholder="New class name" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="fine_button">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            @endif

        </div>
    </div>
</div>

@endsection
