@extends('layouts.master')

@section('main_content')




<div class="container" ng-app="UploadApp" ng-controller="UploadCtrl">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Upload Product</h1>
            <br/>
            <br/>

            <br/>
            <br/>



                    Compound description: <input type="file" id="fileInput" on-read-file="ImportContent($fileContent)">
                    <input type="file" webkitdirectory directory multiple />

                    <br/>
                    <br/>

                    <!-- Structure image: <input type="file" id="imgInput" on-load-image="ImportImg($imageFile)"> -->

                    <br/>
                    <br/>

                    Compound structure image: <img id="imgPreview"></img>

                    <br/>
                    <br/>

                    <p id="fileDisplayArea"></p>

                    <table class="table table-default">
                        <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Name</th>
                            <th>CAS</th>
                            <th>Purity</th>
                        </tr>
                        </thead>
                        <tbody id="filedata">

                        </tbody>
                    </table>






        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
<script src="js/FileReader.js"></script>
<script src="js/upload.js"></script>

@endsection
