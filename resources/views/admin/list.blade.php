@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <br/>
            <br/>
            <h1>List of {{ $modelname }}</h1>
            <br/>
            <br/>

            <br/>
            <br/>

            <a href="{{ '/' . $modelname . '/create' }}">Create new</a></br></br>


            <table class="table table-default">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>
                                {{ $item->name }}
                            </td>
                            <td>
                                <div>

                                <a href="{{ '/' . $modelname . '/' . $item->id . '/edit' }}">Edit</a>|


                                <a onclick="event.preventDefault();
                                            document.getElementById('delete-form{{ $item->id }}').submit();">
                                    Delete
                                </a>

                                <form id="delete-form{{ $item->id }}" action="{{ '/' . $modelname . '/' . $item->id }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>


                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @if(method_exists($items, 'appends'))
        {{ $items->appends(Request::only('query'))->links() }}
    @endif
</div>


@endsection
