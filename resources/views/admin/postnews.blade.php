@extends('layouts.master')

@section('main_content')

<div class="container" ng-app="UploadApp" ng-controller="UploadCtrl">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Post news</h1>
            <br/>
            <br/>



            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ isset($news) ? '/news/' . $news->id : '/news' }}">
                    {{ csrf_field() }}
                    @if(isset($news)) {{ method_field('PUT') }} @endif


                    <div class="form-group">
                        <input id="subject" type="title" class="form-control" name="title" value="{{ isset($news) ? $news->title : '' }}"
                        placeholder="Title" required autofocus>
                    </div>

                    <div class="form-group">
                        <textarea id="body" type="text" class="form-control" name="body"
                        placeholder="Text" required>{{ isset($news) ? $news->body : '' }}</textarea>
                    </div>

                    <img style="max-width:300px" id="imgPreview"></img>

                    Select image: <input type="file" name="image" id="image" on-load-image="ImportImg($imageFile)"></br></br>

                    <div class="form-group">
                        <button type="submit" class="fine_button">
                            Post
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@endsection

@section('includes')
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
    <script src="/js/FileReader.js"></script>
    <script src="/js/upload.js"></script>
@endsection
