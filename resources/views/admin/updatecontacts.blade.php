@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Update Contacts</h1>
            <br/>
            <br/>

            <br/>
            <br/>

            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="POST" action="/updatecontacts">
                    {{ csrf_field() }}

                    <br/><br/>
                    <b>Contacts:</b>
                    <br/>

                    <div class="row">
                        @if(isset($contacts))
                            <div class="col-md-6">
                                @for($i = 0; $i < 6; $i++)
                                    <div class="form-group">
                                        @if(count($contacts) > $i)
                                            {{ Form::text('contactnames[' . $i . ']', $contacts[$i]['name'], array('placeholder'=>'Contact name')) }}<br/>
                                        @else
                                            {{ Form::text('contactnames[' . $i . ']', '', array('placeholder'=>'Contact name')) }}<br/>
                                        @endif
                                    </div>
                                @endfor
                            </div>
                            <div class="col-md-6">
                                @for($i = 0; $i < 6; $i++)
                                    <div class="form-group">
                                        @if(count($contacts) > $i)
                                            {{ Form::text('contactvalues[' . $i . ']', $contacts[$i]['body'], array('placeholder'=>'Contact value')) }}<br/>
                                        @else
                                            {{ Form::text('contactvalues[' . $i . ']', '', array('placeholder'=>'Contact value')) }}<br/>
                                        @endif
                                    </div>
                                @endfor
                            </div>
                        @endif
                    </div>

                    <br/>

                    <div class="form-group">
                        <button type="submit" class="fine_button">
                            Save
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection
