@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Update About</h1>
            <br/>

            <form class="form-horizontal" role="form" method="POST" action="/updateabout">
                {{ csrf_field() }}

                <br/><br/>
                <b>About paragraphs:</b><br/><br/>
                (To make paragraph without title, type 'none' in title field)
                <br/><br/><br/>



                @for($i = 0; $i < 5; $i++)
                    <div class="form-group">
                        @if(count($aboutpars) > $i)
                            {{ Form::text('parnames[' . $i . ']', $aboutpars[$i]['name'], array('placeholder'=>'Paragraph title')) }}<br/>
                        @else
                            {{ Form::text('parnames[' . $i . ']', '', array('placeholder'=>'Paragraph title')) }}<br/>
                        @endif
                    </div>

                    <br/>
                    <div class="form-group">
                        @if(count($aboutpars) > $i)
                            {{ Form::textarea('parbodies[' . $i . ']', $aboutpars[$i]['body'], array('placeholder'=>'Paragraph body')) }}<br/>
                        @else
                            {{ Form::textarea('parbodies[' . $i . ']', '', array('placeholder'=>'Paragraph body')) }}<br/>
                        @endif
                    </div>
                    <br/><br/><br/>
                @endfor

                <br/>

                <div class="form-group">
                    <button type="submit" class="fine_button">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
