@extends('layouts.master')

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Update Slide Text</h1>
            <br/>

            <form class="form-horizontal" role="form" method="POST" action="/updateslidetexts">
                {{ csrf_field() }}

                <br/><br/>
                <b>Text of slides:</b>
                <br/><br/><br/>



                @for($i = 0; $i < 3; $i++)
                    Slide nr.: {{ $i + 1 }}
                    <br/>
                    <div class="form-group">

                        @if(count($slidetexts) > $i)
                            {{ Form::textarea('slidetexts[' . $i . ']', $slidetexts[$i]['body'], array('placeholder'=>'Slide text')) }}<br/>
                        @else
                            {{ Form::textarea('slidetexts[' . $i . ']', '', array('placeholder'=>'Slide text')) }}<br/>
                        @endif

                    </div>
                    <br/><br/>
                @endfor

                <br/>

                <div class="form-group">
                    <button type="submit" class="fine_button">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
