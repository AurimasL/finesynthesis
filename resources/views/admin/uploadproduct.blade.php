@extends('layouts.master')

@section('main_content')

<div class="container" ng-app="UploadApp" ng-controller="UploadCtrl">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <br/>
            <br/>
            <h1>Upload Product</h1>
            <br/>
            <br/>

            <br/>
            <br/>

            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="/products{{ isset($product) ? '/' . $product->id : '' }}">
                    {{ csrf_field() }}
                    @if(isset($product)) {{ method_field('PUT') }} @endif

                    Select image: <input type="file" name="image" id="image" on-load-image="ImportImg($imageFile)"></br></br>
                    <img style="max-width:300px" id="imgPreview"></img>

                    Select product spec: <input type="file" name="productpage"></br></br>

                    <div class="form-group">
                        <input id="name" type="text" class="form-control" name="name" value="{{ isset($product) ? $product->name : '' }}"
                        placeholder="Product name" required>
                    </div>

                    <div class="form-group">
                        <input id="cas" type="text" class="form-control" name="cas" value="{{ isset($product) ? $product->CAS : '' }}"
                        placeholder="CAS">
                    </div>

                    <div class="form-group">
                        <input id="purity" type="text" class="form-control" name="purity" value="{{ isset($product) ? $product->purity : '' }}"
                        placeholder="Purity" required>
                    </div>

                    <b>Classes:</b>

                    @foreach($classes as $class)
                            @if(isset($product) && isset($product->classes()->where('name', $class->name)->first()->id))
                                <br/><label>{{ Form::checkbox('compclasses[' . $class->id . ']', 1, true) }} {{ $class->name }}</label>
                            @else
                                <br/><label>{{ Form::checkbox('compclasses[' . $class->id . ']') }} {{ $class->name }}</label>
                            @endif
                    @endforeach

                    <br/><br/>
                    <b>Prices:</b>
                    <br/>

                    @for($i = 0; $i < 5; $i++)
                        <div class="form-group">
                            @if(isset($product) && count($prices) > $i)
                                {{ Form::text('prices[' . $i . ']', $prices[$i]['price'], array('placeholder'=>'Price')) }}<br/>
                            @else
                                {{ Form::text('prices[' . $i . ']', '', array('placeholder'=>'Price')) }}<br/>
                            @endif
                        </div>
                    @endfor

                    <br/>

                    <div class="form-group">
                        <button type="submit" class="fine_button">
                            Save
                        </button>
                    </div>
                </form>

                @if(isset($product))
                    <form class="form-horizontal" role="form" method="POST" action="/products/{{ $product->id }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <button type="submit" class="fine_button">
                                Delete
                            </button>
                        </div>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
<script src="/js/FileReader.js"></script>
<script src="/js/upload.js"></script>

@endsection
