<style>
    .cookie-consent{
        background-color: purple;
        color: white;
    }

    .cookie-consent__message{
        padding: 5px;
    }

    .fine_button{
        color: #eee;
        background-color: #333;
        border: none;
        padding: 7px 20px 7px 20px;

        margin-top: 8px;
        margin-bottom: 8px;
    }
</style>

<div class="js-cookie-consent cookie-consent">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="cookie-consent__message">
                    {!! trans('cookieConsent::texts.message') !!}
                </div>
            </div>

            <div class="col-md-2 text-center">
                <button class="js-cookie-consent-agree fine_button">
                    {{ trans('cookieConsent::texts.agree') }}
                </button>
            </div>
        </div>
    </div>
</div>
