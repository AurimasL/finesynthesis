@extends('layouts.master')

@section('main_content')

<style>
    .fine_contact_list > li {
        font-size: 12pt;
        padding-bottom: 10px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="page-header">
                <h1>Contacts</h1>
            </div>

            <ul class="fine_contact_list">
                @foreach($contacts as $contact)
                    <li><b>{{ $contact->name }}:</b> {{ $contact->body }}</br></li>
                @endforeach
            </ul>

            </br>
            </br>

            <h3>Send message</h3>
            <br/>

            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="POST" action="/sendmail">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input id="subject" type="text" class="form-control" name="subject" placeholder="Subject" required>
                    </div>

                    <div class="form-group">
                        <input id="email" type="email" class="form-control" name="email" placeholder="Your e-mail" required>
                    </div>

                    <div class="form-group">
                        <textarea id="message" type="text" class="form-control" name="message" required></textarea>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="fine_button">
                            Send
                        </button>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

@endsection

@section('includes')



@endsection
