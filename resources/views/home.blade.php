@extends('layouts.master')

@section('main_content')

<div class="slideshow">
    <div class="slide-container">
        <div class="mySlides">
            <img class="slideImg" src="images/11.jpg">
            @if(isset($slidetexts) && count($slidetexts) > 0)
                <div class="text">
                    <div class="jumbotron">
                        {{ $slidetexts[0]->body }}
                    </div>
                </div>
            @endif
        </div>

        <div class="mySlides">
            <img class="slideImg" src="images/41.jpg">
            @if(isset($slidetexts) && count($slidetexts) > 1)
                <div class="text">
                    <div class="jumbotron">
                        {{ $slidetexts[1]->body }}
                    </div>
                </div>
            @endif
        </div>

        <div class="mySlides">
            <img class="slideImg" src="images/51.jpg">
            @if(isset($slidetexts) && count($slidetexts) > 2)
                <div class="text">
                    <div class="jumbotron">
                        {{ $slidetexts[2]->body }}
                    </div>
                </div>
            @endif
        </div>

        <div class="container-fluid fine_search">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <form class="form-horizontal" role="form" method="GET" action="\searchresults">
                        {{ csrf_field() }}

                        <div class="input-group input-group-lg">

                            <input type="text" name="query" class="form-control" placeholder="Enter CAS or name of a compound">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Search</button>
                            </span>

                        </div><!-- /input-group -->
                    </form>
                </div>
            </div>
        </div>

        <div class="container-fluid slideshow_dots">
            <div class="row">
                <span class="dot"></span>
                <span class="dot"></span>
                <span class="dot"></span>
            </div>
        </div>
    </div>
    <br>
</div>

@endsection

@section('includes')

    <script src="/js/slideshow.js" type="text/javascript"></script>
    <link href="/css/slideshow.css" rel="stylesheet" type="text/css">

@endsection
