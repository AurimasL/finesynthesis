<?php

use Illuminate\Database\Seeder;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        DB::table('infos')->insert([
            'type' => 'about',
            'name' => $faker->realText($maxNbChars = 50, $indexSize = 2),
            'body' => $faker->realText($maxNbChars = 350, $indexSize = 2)
        ]);

        DB::table('infos')->insert([
            'type' => 'about',
            'name' => 'none',
            'body' => $faker->realText($maxNbChars = 350, $indexSize = 2)
        ]);

        DB::table('infos')->insert([
            'type' => 'about',
            'name' => 'none',
            'body' => $faker->realText($maxNbChars = 350, $indexSize = 2)
        ]);
    }
}
