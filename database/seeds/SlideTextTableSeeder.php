<?php

use Illuminate\Database\Seeder;

class SlideTextTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infos')->insert([
            'type' => 'slidetext',
            'name' => 'slide1',
            'body' => 'Chemicals for your research and manufacture needs'
        ]);

        DB::table('infos')->insert([
            'type' => 'slidetext',
            'name' => 'slide1',
            'body' => 'Production of Tolane has been started'
        ]);

        DB::table('infos')->insert([
            'type' => 'slidetext',
            'name' => 'slide1',
            'body' => 'Wide range of precursors'
        ]);
    }
}
