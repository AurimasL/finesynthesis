<?php

use Illuminate\Database\Seeder;

class CompClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $compclasses = [
            'Acetophenones',
            'Hydrocarbons',
            'Anilines',
            'Arylacetylenes',
            'Aryl halides and nitro aryls',
            'Benzoic acids and derivatives',
            'Phenols',
            'Miscellaneous',
            'Piridines, pirimidines, chinolines',
            'Tiophene derivatives',
            'Liquid cristals',
            'Sulfoacids, sulfochlorides'
        ];

        foreach($compclasses as $compclass)
        {
            DB::table('comp_classes')->insert([
                'name' => $compclass,
            ]);
        }


    }
}
