<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        // $this->call(CompClassesTableSeeder::class);
        $this->call(CompoundsTableSeeder::class);
        // $this->call(SlideTextTableSeeder::class);
        // $this->call(NewsTableSeeder::class);
        // $this->call(AboutTableSeeder::class);
        //$this->call(ContactsTableSeeder::class);
    }
}
