<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        DB::table('infos')->insert([
            'type' => 'contact',
            'name' => 'Phone',
            'body' => '+3706' . $faker->unique()->regexify('[0-9]{6}')
        ]);

        DB::table('infos')->insert([
            'type' => 'contact',
            'name' => 'Phone',
            'body' => '+3706' . $faker->unique()->regexify('[0-9]{6}')
        ]);

        DB::table('infos')->insert([
            'type' => 'contact',
            'name' => 'E-mail',
            'body' => $faker->email()
        ]);

        DB::table('infos')->insert([
            'type' => 'contact',
            'name' => 'E-mail',
            'body' => $faker->email()
        ]);
    }
}
