<?php

use Illuminate\Database\Seeder;
use App\Fine\Product\ProductPageModifier;

class CompoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $productPageModifier = null;

    private $descriptionPath = null;
    private $imgPath = null;
    private $appImgPath = "/compounds/images/";

    public function __construct(ProductPageModifier $ppm)
    {
        $this->productPageModifier = $ppm;

        $this->descriptionPath = getcwd() . "/public/compounds/";
        $this->imgPath = getcwd() . "/public/compounds/images/";
    }

    private function addProduct($productName, $classNames, $CAS = null, $purity = null, $prices){
        // Insert compound
        //
        //
        // $id = DB::table('compounds')->insertGetId([
        //     'name' => $productName,
        //     'purity' => $purity,
        //     'CAS' => $CAS,
        // ]);
        //
        // $newCompound = App\Compound::find($id);
        //
        // // Add prices
        // if(is_array($prices)){
        //     foreach ($prices as $price) {
        //         DB::table('prices')->insert([
        //             'compound_id' => $id,
        //             'price' => $price,
        //         ]);
        //     }
        // }else{
        //     DB::table('prices')->insert([
        //         'compound_id' => $id,
        //         'price' => $prices,
        //     ]);
        // }
        //
        // // Add classes
        // if(is_array($classNames)){
        //     foreach ($classNames as $className) {
        //         $newCompound->classes()->attach(DB::table('comp_classes')->where('name', $className)->first()->id);
        //     }
        // }else{
        //     $newCompound->classes()->attach(DB::table('comp_classes')->where('name', $classNames)->first()->id);
        // }
        //
        //
        // // Add values of attributes
        // if(is_array($attrNames) && is_array($attrValues)){
        //     $valueCount = min(count($attrNames), count($attrValues));
        //     for($i = 0; $i < $valueCount; $i++){
        //         $attribute = DB::table('attributes')->where('name', $attrNames[$i])->first();
        //
        //         DB::table('attribute_values')->insert([
        //             'attribute_id' => $attribute->id,
        //             'compound_id' => $id,
        //             'value' => $attrValues[$i],
        //         ]);
        //     }
        // }else{
        //     $attribute = DB::table('attributes')->where('name', $attrNames)->first();
        //
        //     DB::table('attribute_values')->insert([
        //         'attribute_id' => $attribute->id,
        //         'compound_id' => $id,
        //         'value' => $attrValues,
        //     ]);
        // }
        //
        //
        //
        // // Add structural formula
        // DB::table('comp_structures')->insert([
        //     'compound_id' => $id,
        //     'path' => 'images/' . $compoundName . '.png',
        // ]);
        // error_log('done');
    }

    public function dummy(){
        // $this->addCompound('4-n-Nonylbiphenyl', 'Aromatics', array('CAS', 'Boiling point', 'Price', 'Odor', 'Purity'),
        //                                         array('54786-878', '158 C', 'Fidy dolars', 'Pachmel', '98%'),
        //                                         array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('3,5-Dibromobiphenyl', array('Aromatics', 'Halides'), array('CAS', 'Boiling point', 'Price', 'Color', 'Purity'),
        //                                             array('12546-211', '296 C', 'tre fidy', 'Transparent', '99%'),
        //                                             array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('1,2-Trimethylenedioxybenzene', 'Aromatics', array('CAS', 'Odor', 'Price', 'Purity'),
        //                                                                         array('57912-024', 'Awful', 'Cheap', '>99%'),
        //                                                                         array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound("1,1'-Binaphthyl", 'Aromatics', array('CAS', 'Boiling point', 'Price', 'Color', 'Odor', 'Purity'),
        //                                                     array('97781-999', '109 C', '50$', 'Transparent', 'Fruity', '97%'),
        //                                                     array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('3-(4-Methoxybenzoyl)propionic acid', array('Aromatics', 'Acids'),
        //                                                     array('CAS', 'Boiling point', 'Price', 'Color', 'Odor', 'Purity'),
        //                                                     array('66666-616', '299 C', '890$', 'White', 'Cancer inducing', '90%'),
        //                                                     array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('4,4-Dibromo-p-terphenyl', array('Halides', 'Aromatics'), array('CAS', 'Purity'),
        //                                                                             array('77777-77', '>99%'),
        //                                                                             array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('1-(4-Chloromethyl)-2-phenylethane', array('Halides', 'Aromatics'),
        //                                                     array('CAS', 'Boiling point', 'Price', 'Purity'),
        //                                                     array('54876-211', '125 C', '51$', '98%'),
        //                                                     array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('1,2-Diphenylethane', 'Aromatics', array('CAS', 'Boiling point', 'Price', 'Purity'),
        //                                                     array('57469-125', '325 C', '127$', '97%'),
        //                                                     array('100g - 50$', '500g - 200$', '> 1kg'));
        //
        // $this->addCompound('1,3-Dibromo-5-hexylbenzene', array('Aromatics', 'Halides'), array('CAS', 'Price', 'Purity'),
        //                                                                             array('12015-998', '98$', '97%'),
        //                                                                             array('100g - 50$', '500g - 200$', '> 1kg'));
    }

//     public function run()
//     {
//         $filedata = "(2-Pyrimidylthio)acetic acid|88768-45-0|>99%
// (3,5-Diphenylphenyl)boronic acid|128388-54-5| >98%
// 1,1'-Binaphthyl|604-53-5|>98 %
// 1,1,2,2-Tetraphenylethane|632-50-8|> 99 %
// 1,1,3,3-Tetraethoxy-2-methylpropane|10602-37-6|>98% (GC)
// 1,1-Diethoxy-2-(diethoxymethyl)butane|30989-69-6|>98% (GC)
// 1,1-Diphenyl-1-butene|1726-14-3 |> 98 %
// 1,2,3,4-Tetramethylbenzene|488-23-3|> 98%
// 1,2-Di-dodecyl-4,5-diiodobenzene|181144-56-9|>98 %
// 1,2-Diphenylethane|103-29-7|>99 %
// 1,2-Trimethylenedioxybenzene|7216-18-4|>98%
// 1,3-Butylene sulfate|4426-50-0|>98%
// 1,3-Dibromo-5-ethylbenzene|59785-43-2|>98%
// 1,3-Dibromo-5-hexylbenzene|75894-97-2|>98%
// 1,4,6,7-Tetramethylnaphthalene|13764-18-6| >98%
// 1,4-Benzodioxan-6-carboxaldehyde|29668-44-8|> 98%
// 1,4-Benzodioxan-6-yl methyl ketone|2879-20-1|98%
// 1,4-Benzodioxane|493-09-4|>99% (GC)
// 1,4-Dibromo-2,3-dimethylnaphthalene|19930-62-2|>99%
// 1,4-Dibromo-2,5-didodecylbenzene|117635-23-1|>98 %
// 1,4-Dibutylbenzene|1571-86-4|>98 %
// 1,4-Dicyanonaphthalene|3029-30-9|>98 %
// 1,4-Dimethoxynaphthalene|10075-62-4|>98 %
// 1-(4-Chloromethyl)-2-phenylethane|80676-35-3|>98%
// 1-(4-Nitrophenyl)glycerol|2207-68-3; 41204-85-7| >99%
// 1-Aminoanthracene|610-49-1| >98 %
// 1-Bromo-2,3,5,6-tetramethylbenzene|1646-53-3.|> 98%
// 1-Bromo-2-hexyloctane|115007-16-4|>98% (GC)
// 1-Bromo-2-methyl-4-hexylbenzene|Not found.|>98%
// 1-Bromo-3,5-di-hexyl-benzene|1238156-36-9|>98%
// 1-Bromo-3-butylbenzene| 54887-20-6|>99 %
// 1-Bromo-3-ethyl-5-hexylbenzene|Not found.|>98%
// 1-Bromo-3-hexanoylbenzene|1041467-43-9|>98 %
// 1-Bromo-3-hexylbenzene|38409-59-5|>97 %
// 1-Bromo-3-n-octylbenzene|663949-34-6|>98%
// 1-Bromo-3-octanoylbenzene|Not found.|>98 %
// 1-Bromo-4-cyclohexylbenzene|25109-28-8|> 99%
// 1-Bromo-4-eicosanylbenzene|1293366-53-6| >98 %
// 1-Bromo-4-hexadecylbenzene|2196-39-8.| >98 %
// 1-bromo-4-n-dodecylbenzene|126930-72-1|>98 %
// 1-Bromo-4-n-heptylbenzene|76287-49-5|>98%
// 1-Bromo-4-n-hexylbenzene|23703-22-2|>99%
// 1-Bromo-4-n-hexyloxybenzene| 30752-19-3|>98%
// 1-Bromo-4-n-octylbenzene|51554-93-9|>98%
// 1-Bromo-4-n-octyloxybenzene|96693-05-9|>98%
// 1-Bromo-4-n-pentylbenzene|51554-95-1| >99% (GC)
// 1-Bromo-4-n-propylbenzene|588-93-2|>98%
// 1-Bromo-4-nonylbenzene|51554-94-0|>98%
// 1-Bromo-4-octadecylbenzene|Not reported.|>98 %
// 1-Bromo-4-pentyloxybenzene| 30752-18-2|>98%
// 1-Bromo-4-tetradecylbenzene|113584-25-1| >98 %
// 1-Bromopyrene|1714-29-0|>97%
// 1-Butenyl ethyl ether|929-05-5|98 % (sum of cis- and trans-isomers)
// 1-Butoxy-4-ethynylbenzene|79887-15-3| >98 %
// 1-Butyl-4-iodobenzene|20651-67-6|>98% (GC)
// 1-Chloro-2-iodoethane|624-70-4|>98% (GC)
// 1-Chloroisoquinoline|19493-44-8|>99 %
// 1-Dodecylphosphonic acid|5137-70-2|>95 %
// 1-Ethoxy-1-pentene|5909-75-1|>98%
// 1-Ethynyl-3-methylbenzene|766-82-5|>97%
// 1-Ethynyl-4-fluorobenzene|766-98-3|>99%
// 1-Ethynyl-4-methoxybenzene|768-60-5| >99%
// 1-Ethynyl-4-n-propylbenzene|62452-73-7|>99%
// 1-Heptyl-4-iodobenzene|131894-91-2|>98% (GC)
// 1-Hexyl-4-iodobenzene|62150-34-9|>98% (GC)
// 1-Iodo-4-n-pentylbenzene|85017-60-3|>98% (GC)
// 1-Iodo-4-n-propylbenzene|126261-84-5|>98% (GC)
// 1-Iodonaphthalene|90-14-2|>99 %
// 1-Methyl-1H-pyrazole-4-sulfonyl chloride|288148-34-5|>98%
// 1-Nitro-4-propylbenzene|10342-59-3|>96% (GC)
// 1-Phenylheptane|1078-71-3| >98 %
// 1-tert-Butyl-4-iodobenzene|35779-04-5|>97% (GC)
// 10,10'-Dibromo-9,9'-bianthracene|121848-75-7|>99%
// 10-Undecynecarboxylic acid|2777-65-3|> 98% (GC)
// 2'-Iodo-m-terphenyl|82777-09-1|> 99%
// 2,2'-Bicinchoninic acid|1245-13-2|>98 %
// 2,2'-Bipyridine-4,4'-dicarboxylic acid|6813-38-3|>98%
// 2,2'-Bipyridine-6,6'-dicarboxylic acid|4479-74-7|>99%
// 2,2'-Dibromobiphenyl|13029-09-9|>98 %
// 2,2'-Dimethyl-4,4'-diiodobiphenyl|69571-02-4|>98 %
// 2,2'-Dimethylbibenzyl|952-80-7|>98 %
// 2,2,4,7-Tetramethyl-1,2,3,4-tetrahydroquinoline|59388-58-8|> 97%
// 2,3,4,5-Tetramethylaniline|2217-45-0|>98%
// 2,3,5,6-Tetramethylaniline|2217-46-1|>98%
// 2,3,5-Triphenylpyrazine|36476-77-4| > 98%
// 2,3-Dichloro-4-butyrylphenoxyacetic acid|58574-03-1|>98%
// 2,3-Dimethylnaphthalene|581-40-8|
// 2,4-Dibromo-6-nitroaniline|827-23-6|>99 %
// 2,4-Dichloro-6-phenylthieno{3,2-d}pyrimidine|36926-41-7|> 98%
// 2,4-Dichlorobenzo{h}quinazoline|Not reported.|> 98 %
// 2,4-Diiodomesitylene|53779-84-3|> 98%
// 2,4-Dimethylquinoline| 1198-37-4|>98%
// 2,4-Ethen-di-yl-bipyridine|14802-41-6|>98%
// 2,4-Quinolinediol|86-95-3| > 98%
// 2,5-Dibromo-m-xylene|100189-84-2|> 98%
// 2,5-Dibromo-para-xylene|1074-24-4|>98 %.
// 2,5-Dichloro-p-xylene|1124-05-6|>98 %
// 2,5-Dichloroterephthalic acid|13799-90-1|>98 %
// 2,5-Dinitro-para-xylene|712-32-3|> 98%
// 2,6-Di(4-methylphenyl)pyridine|14435-88-2|>98%
// 2,6-Di-tert-butyl-1,4-benzoquinone|719-22-2|>98% (GC)
// 2,6-Di-tert-butylpyridine|585-48-8|>98 % (GC)
// 2,6-Dibromodithienothiophene|67061-69-2|> 98%
// 2,6-Difluoro-4-iodoaniline|141743-49-9|>98 %
// 2,6-Dimethyl-4-butylaniline|Not found.|>99%
// 2,6-Dimethyl-4-hexylaniline|Not found.|>98%
// 2,6-Dimethyl-4-tert-butylaniline|42014-60-8|>98%
// 2,6-Dimethyl-4-tert-butylbenzoyl chloride|119335-81-8|> 98%
// 2,6-Dimethylpyridin-3-ol|1122-43-6|
// 2,6-Diphenyl-4-aminophenol|50432-01-4| >98% (HPLC)
// 2,6-Diphenyl-4-nitrophenol|2423-73-6|>98% (GC)
// 2,6-Diphenylpyridine|3558-69-8|> 97 %
// 2,6-Pyridinedicarbonyl dichloride|3739-94-4|>98% (GC)
// 2,7-Dibromo-9-fluorenone|14348-75-5|>99 %
// 2,7-Dioctylfluorenone|Not found.|>99 %
// 2,7-Diphenyl-9,9-bis-(4-aminophenyl)-9H-fluorene|Not reported.|> 98 %
// 2,8-Dibromodibenzothiophene|31574-87-5|>98%
// 2-(4-Ethylphenyl)-5-propylpyrimidine|98495-11-5|> 99.9 %
// 2-(5-Bromobiphenyl-3-yl)pyridine|1268251-04-2|> 98%
// 2-(p-Tolyl)pyridine|4467-06-5.|>98% (GC)
// 2-Aminoanthracene|613-13-8| >97 %
// 2-Benzylideneindan-1,3-dione|5381-33-9|> 98 %
// 2-Bromo-13,13-dimethylindenoanthracene|1258514-99-6|> 99 %
// 2-Bromo-5-tert-butyl-1,3-dimethylbenzene|5345-05-1|>98 %
// 2-Bromo-9,10-diphenylanthracene|20173-79-5|>98%
// 2-Bromobiphenyl|2052-07-5|>98 %
// 2-Chloro-5-decylpyrimidine|170434-06-7|> 98%
// 2-Chloro-5-ethylpyrimidine|111196-81-7|>98%
// 2-Chloro-5-n-pentylpyrimidine|154466-62-3|> 98%
// 2-Chloro-5-propylpyrimidine|219555-98-3.|>99%.
// 2-Chlorolepidine|634-47-9|>99%
// 2-Cyclohexylbenzoic acid|97023-48-8|> 98%
// 2-Diethoxymethyl-1,1-diethoxypentane|21037-61-6| >98%
// 2-Ethylbenzoic acid|612-19-1|>98%
// 2-Ethynylmesitylene|769-26-6|>98%
// 2-Iodobiphenyl|2113-51-1|>98 %
// 2-Mercapto-5-propylpyrimidine|52767-84-7|> 98%
// 2-Methyl-4-n-hexylaniline|Not reported.|>98%
// 2-Methylstyrene|611-15-4.|98%
// 2-Pentylanthraquinone|13936-21-5.|>98%
// 2-Phenylanthracene|1981-38-0|>99%.
// 2-Phenylanthraquinone|6485-97-8|>98%
// 2-Phenylpyridine|1008-89-5.|>98%
// 2-Propylbenzoic acid|2438-03-1|>98 %
// 2-Propylresorcinol|13331-19-6| >98%
// 2-tert-Butylanthracene|18801-00-8|> 98%
// 3',4'-Methylenedioxyacetophenone|3162-29-6|>98% (GC)
// 3,3''-Dibromo-m-terphenyl|95962-62-2|>98% (GC)
// 3,3'-Diaminobiphenyl|2050-89-7|>98% (GC)
// 3,3'-Dibromobenzophenone|25032-74-0|>98%
// 3,3'-Dibromobiphenyl|16400-51-4|>98% (GC)
// 3,3'-Dibromotolane|91790-32-8; 153404-60-5|>99 %
// 3,3'-Dimethyl-4,4'-bis-(4-methylphenylamino)biphenyl|Not reported.|> 98%
// 3,3'-Dimethylbiphenyl|612-75-9|>98%
// 3,3-Dimethyl-1-indanone|26465-81-6|> 98%
// 3,4,5,6-Tetrachloro-1,2-benzoquinone|2435-53-2|> 98 %
// 3,4-(Trimethylenedioxy)aniline|175136-34-2| > 98%
// 3,4-(Trimethylenedioxy)benzoic acid|33632-74-5.|>98%
// 3,4-Trimethylenedioxyacetophenone|22776-09-6|>98%
// 3,4-Trimethylenedioxyphenacyl bromide|35970-34-4|>96%
// 3,5-Di-tert-butyl-1,2-benzoquinone|3383-21-9|>98% (GC)
// 3,5-Di-tert-butyl-2-hydroxybenzaldehyde|37942-07-7|>99%
// 3,5-Di-tert-butyl-4-hydroxy-phenylacetic acid|1611-03-6|>98%
// 3,5-Di-tert-butyl-4-hydroxybenzaldehyde|1620-98-0.| >98%
// 3,5-Di-tert-butyl-4-hydroxybenzoic acid|1421-49-4|>98%
// 3,5-Di-tert-butyl-4-hydroxybenzonitrile|1988-88-1|>98%
// 3,5-Di-tert-butyl-4-hydroxyphenacyl bromide|14386-64-2|>97%
// 3,5-Di-tert-butyl-4-hydroxyphenylacetonitrile|1611-07-0|>98%
// 3,5-Dibromobenzonitrile|97165-77-0|> 98%
// 3,5-Dibromobiphenyl|16372-96-6|>99%
// 3,5-Dibromotoluene|1611-92-3|>98%
// 3,5-Dichloro-2-pyridone|5437-33-2.| >99%
// 3,5-Dimethyl-4-hydroxybenzaldehyde|2233-18-3|>98%
// 3,6-Dibromofluorenone|216312-73-1|>98 %
// 3-(4-Butylbenzoyl)propanoic acid|72271-71-7|98+ %
// 3-(4-Ethoxybenzoyl)propionic acid|53623-37-3|98+ %
// 3-(4-Ethylbenzoyl)propionic acid|49594-75-4|98+ %
// 3-(4-Methoxybenzoyl)propionic acid|3153-44-4|98+ %
// 3-(Bromomethyl)biphenyl|14704-31-5|>98% (GC)
// 3-Acetyl-2,4,6-trimethylpyridine|56704-25-7|>98 %
// 3-Aminobiphenyl|2243-47-2|>99 %
// 3-Bromo-2,4,6-trimethyl-1,1'-biphenyl|20434-39-3|> 98 %
// 3-Bromo-2-ethoxypyridine|57883-25-7|>97%
// 3-Bromo-3'-hexylbiphenyl|Not found.|>98 %
// 3-Bromo-4'-aminobiphenyl|91394-64-8|>98% (GC)
// 3-Bromo-4'-hexylbiphenyl|Not found.|>98 %
// 3-Bromo-4'-iodobiphenyl|187275-73-6|>98% (GC)
// 3-Bromo-4-ethylphenol|540495-28-1|> 98%
// 3-Bromobenzophenone|1016-77-9|>98 %
// 3-Bromobiphenyl|2113-57-7|>98 %
// 3-Bromodibenzofuran|26608-06-0|>99.5%
// 3-Bromofluorenone|2041-19-2|>98 %
// 3-Ethylbenzoic acid|619-20-5|>98 %
// 3-Ethylbiphenyl|5668-93-9| 99 %
// 3-Iodo-N-phenylcarbazole|502161-03-7| >99 % (GC)
// 3-Methylphenyl-phenylfluorenyl-amine|1292285-22-3| >98 %
// 3-Nitrostyrene|586-39-0|> 97%
// 3-Phenyl-1-indanone|16618-72-7|>98%
// 3-Phenyl-2-oxazolidinone|703-56-0|> 98%
// 3-Phenylindole|1504-16-1|> 98% (GC)
// 4'-Bromobiphenyl-4-amine|3365-82-0|>98%
// 4'-Bromooctanophenone|7295-48-9|>98% (GC)
// 4'-Nonylacetophenone|37593-05-8|>98%
// 4,4'-Bis(bromomethyl)biphenyl|20248-86-6|>98%
// 4,4'-bis-(4-Hexyloxystyryl)-2,2'-bipyridine|846563-66-4|>98 %
// 4,4'-Bis-methoxycarbonyl-2,2'-bipyridine|71071-46-0|>98%
// 4,4'-di-tert-Butylbiphenyl|1625-91-8| 99%
// 4,4'-Diacetylbiphenyl|787-69-9|> 98 %
// 4,4'-Dibromo-2-nitrobiphenyl|439797-69-0|> 98%
// 4,4'-Dibromobenzil|35578-47-3|>97 %
// 4,4'-Dibromobenzophenone|3988-03-2.|>98 %
// 4,4'-Dibromobibenzyl|19829-56-2|>98% (GC)
// 4,4'-Dibromodiphenic acid|54389-67-2|>98 %
// 4,4'-Dibromotolane|2789-89-1|>99 %
// 4,4'-Dicyano-2,2'-bipyridine|67491-43-4|>98%
// 4,4'-Diiodo-3,3'-dimethylbiphenyl|7583-27-9|>98 %
// 4,4'-Dimethoxytolane|2132-62-9.|>99 %
// 4,4'-Dimethyl-2,2'-dipyridyl- Specification|1134-35-6|>99 %
// 4,4'-Dinonyl-2,2'-bipyridine|142646-58-0|>98 %
// 4,4'-Diphenylstilbene|2039-68-1|>99 %
// 4,4-Dibromo-p-terphenyl-Specification|17788-94-2|>99.8% (GC)
// 4-(2-Phenethyl)pyridine|2116-64-5.|>98 %
// 4-(3-Chlorophenyl)pyridine|5957-92-6| >98%
// 4-(4-Pyridyl)benzoic acid|4385-76-6|>98%
// 4-(Bromomethyl)biphenyl|2567-29-5|>96% (GC)
// 4-(Trifluoromethyl)indan-1-one|68755-42-0|> 98%
// 4-(Trimethylsilyl)phenylacetylene|16116-92-0|>98%
// 4-Aminobenzoylhydrazine|5351-17-7| >98%
// 4-Bromo-1,2-benzenedimethanol|171011-37-3|> 98%
// 4-Bromo-2-nitrophenol|7693-52-9|>98 %
// 4-Bromo-3,5-dimethyl-p-terphenyl|Not found.|>98 %
// 4-Bromo-3,5-dimethylbiphenyl|756873-19-5|>98%
// 4-Bromo-3-ethylbenzonitrile|170230-29-2|>98 %
// 4-Bromo-4-tert-butylbiphenyl|162258-89-1|>99%
// 4-Bromo-N,N-dibutylaniline|53358-54-6|>98%
// 4-Bromo-N,N-dihexylaniline|425604-49-5|>98%
// 4-Bromophthalic acid|6968-28-1|>98%
// 4-Bromostyrene - Spec|2039-82-9|>97%
// 4-Bromostyrene oxide|32017-76-8|>98% (GC)
// 4-Butoxybenzaldehyde|5736-88-9|>98% (GC)
// 4-Butyl-2-methylaniline|72072-16-3|>98%
// 4-Chloro-1(2H)-phtalazinone|2257-69-4|> 98%
// 4-Chlorostyrene oxide|2788-86-5|>98% (GC)
// 4-Cyano-4''-pentylterphenyl|54211-46-0| 99%.
// 4-Cyano-4'-n-decylbiphenyl|59454-35-2| 99%
// 4-Cyano-4'-n-dodecylbiphenyl|57125-49-2| >99 %
// 4-Cyano-4'-nonylbiphenyl|52709-85-0| 99%
// 4-Cyano-4'-undecylbiphenyl| 65860-74-4.|>99 %
// 4-Cyclohexylbenzoic acid|20029-52-1|> 98 %
// 4-Decyloxy-4'-cyanobiphenyl|70247-25-5| 99%
// 4-Ehylaminobenzoic acid|7409-09-8.|>98%
// 4-Ethoxycinnamic acid|2373-79-7|>98% (HPLC)
// 4-Ethoxyphenylacetylene|79887-14-2| >98 %
// 4-Ethylbenzenesulphonyl chloride|16712-69-9|> 98%
// 4-Ethylbenzonitrile|25309-65-3|>98 %
// 4-Ethylbenzoyl chloride|16331-45-6.|
// 4-Ethylphenylacetylene|40307-11-7|>99%
// 4-Ethynyl-2,6-difluoroaniline|753501-37-0|>99 %
// 4-Ethynyltoluene|766-97-2|>98%
// 4-Fluoro-4'-hydroxybiphenyl|324-94-7|> 98%
// 4-Fluoro-4'-hydroxytolane|197770-48-2| > 98%
// 4-Fluoro-alpha-methylstyrene|350-40-3|98%
// 4-Fluorostyrene oxide|18511-62-1|>98% (GC)
// 4-Fluorostyrene|405-99-2|97%
// 4-Heptylbenzoic acid|38350-87-7| >97 % (GC)
// 4-Heptyloxyphenylacetylene|79887-18-6| >98 %
// 4-Heptylpyridine|40089-90-5|>98 %
// 4-Hexyl-1-iodo-2-methylbenzene|Not found.|>98%
// 4-Hexyloxyphenylacetylene|79887-17-5.| >98 %
// 4-Isopropylphenylacetic acid|4476-28-2|>98%
// 4-Methoxytolane|7380-78-1|>99 %
// 4-Methyl-2,2'-bipyridyl|56100-19-7|>98 %
// 4-n-Butylacetophenone|37920-25-5|>98% (GC)
// 4-n-Butylbenzenesulphonyl chloride| 54997-92-1|98%
// 4-n-Butylbenzoyl chloride|28788-62-7|> 99%
// 4-n-Butylbiphenyl|37909-95-8|> 99%
// 4-n-Butylphenylacetylene|79887-09-5|>99 %
// 4-n-Decylaniline|37529-30-9|>98%
// 4-n-Dodecylbenzoic acid|21021-55-6|> 98 %
// 4-n-Dodecylbenzoyl chloride|53097-40-8|>98%
// 4-n-Heptylacetophenone|37593-03-6|>98%
// 4-n-Heptylaniline|37529-27-4|>98%
// 4-n-Heptylbenzoic acid|38350-87-7| >98% (GC)
// 4-n-Heptylbiphenyl| 59662-32-7| 99%
// 4-n-Heptylphenylacetylene|79887-12-0|>98%
// 4-n-Hexylacetophenone|37592-72-6|>98%
// 4-n-Hexylaniline|33228-45-4|>98%
// 4-n-Hexylbenzoic acid|21643-38-9|>99%
// 4-n-Hexylbiphenyl|59662-31-6| 99%
// 4-n-Hexylphenylacetylene|79887-11-9|>98%
// 4-n-Nonylaniline|37529-29-6|>98%
// 4-n-Nonylbiphenyl|93972-01-1| 98%
// 4-n-Nonyloxy-4'-cyanobiphenyl|58932-13-1| 99.5%
// 4-n-Nonylphenol|104-40-5|>99%
// 4-n-Octylacetophenone|10541-56-7|>99%
// 4-n-Octylaniline|16245-79-7|>98%
// 4-n-Octylbenzenesulfonic acid sodium salt|6149-03-7|>98%
// 4-n-Octylbenzoic acid|3575-31-3|> 98%
// 4-n-Octylbiphenyl|7116-97-4| 99%
// 4-n-Octylphenol|1806-26-4|>99%
// 4-n-Octylphenylacetylene|79887-13-1|>98%
// 4-n-Pentylacetophenone|37593-02-5|>98%
// 4-n-Pentylaniline|33228-44-3|>98%
// 4-n-Pentylbenzenesulfonyl chloride| 73948-18-2|97%
// 4-n-Pentylbenzoic acid|26311-45-5|>99%
// 4-n-Pentylbenzoyl chloride|49763-65-7|>98%
// 4-n-Pentylbiphenyl|7116-96-3| 99%
// 4-n-Propylbenzenesulphonyl chloride|146949-07-7|98%
// 4-n-Propylbiphenyl|10289-45-9| >98%
// 4-n-Propylphenol|645-56-7|>98%
// 4-Nitrocinnamaldehyde|1734-79-8|>98%
// 4-Nitrocinnamyl alcohol|1504-63-8|>98%
// 4-Nitrophenylacetone|5332-96-7|>98% (GC)
// 4-Nonylpyridine|40089-92-7|>98 %
// 4-Octyl-4'-cyanobiphenyl|52709-84-9| >99 %
// 4-Octylbenzoic acid|3575-31-3|
// 4-Octylpyridine|40089-91-6.|>98 %
// 4-Pentyloxyphenylacetylene|79887-16-4| >99 %
// 4-Phenylpyridine|939-23-1|> 99 % (GC)
// 4-Propoxyphenylacetylene|39604-97-2.| >98 %
// 4-Propylbenzoic acid|2438-05-3|> 99%
// 4-tert-Butyl-2,6-dimethylbenzyl chloride|19387-83-8|>98 %
// 4-tert-Butyl-6-phenylpyrimidine|1373880-78-4|> 98 %
// 4-Tert-butylacetophenone|943-27-1|>98% (GC)
// 4-tert-Butylbenzenesulfonyl chloride| 15084-51-2|> 98%
// 4-tert-Butylbenzoic hydrazide|43100-38-5|>98 %
// 4-Trimethylsilyltolane|136459-72-8| >97%
// 4-Undecyloxy-benzoic acid|15872-44-3|> 98 %
// 4-{(Carbazol-9-yl)phenyl}boronic acid|419536-33-7|>98%
// 4H-Indeno1,2-bthiophen-4-one|5706-08-1|>98%
// 5'-Bromo-m-terphenyl|103068-20-8|>99%
// 5,5'-Dipropylbiphenyl-2,2'-diol|20601-85-8| >98%
// 5,5-Dimethyl-2,2-bipyridine|1762-34-1|>98%
// 5,6-Dibromo-2,1,3-benzothiadiazole|18392-81-9|>98%
// 5,6-Diphenyl-2-(2-pyridyl)triazine|1046-56-6|>99%
// 5,9-Dibromo-7,7-dimethyl-7H-benzofluorene|1056884-35-5|>99 %
// 5-Bromo-1,3-benzenedimethanol|51760-22-6|> 98%
// 5-Bromo-2,2'-bipyridine|15862-19-8|>98%
// 5-Bromo-2-tert-butylpyrimidine|85929-94-8|>99%
// 5-Bromo-2-thiophenecarboxylic acid|7311-63-9|> 99%
// 5-Bromo-4,4''-di-ter-butyl-m-terphenyl|351029-50-0|>99 %
// 5-Bromoacenaphthene|2051-98-1|>98 %
// 5-Butyl-2-(4-ethylphenyl)pyrimidine|64835-59-2|> 99.5 %
// 5-Carbethoxy-2-thiouracil|38026-46-9|> 99%
// 5-Carbethoxyuracil|28485-17-8| >98%
// 5-Ethyl-2-(4-ethylphenyl)pyrimidine|98495-10-4|> 99.9 %
// 5-Ethyl-2-{4-(4-propylcyclohexyl)phenyl}pyrimidine|98495-16-0|> 99.9%
// 5-Hexen-2-one| 109-49-9|(GC) >98 %
// 5-Hexyl-2-iodo-1,3-dimethylbenzene|Not found.|>98 %
// 5-Methyl-2,2'-bipyridine|56100-20-0|>98 %
// 5-Methyl-6-(2-methylphenyl)-4-phenylpyrimidine|1373880-84-2 |> 98 %
// 5-Phenylpentan-1-ol|10521-91-2| > 98%
// 6-Bromo-1,4-benzodioxane|52287-51-1|>98% (GC)
// 6-Bromo-2,2'-bipyridine|10495-73-5|>98%
// 6-Bromo-3,3-dimethyl-indan-1-one|67159-84-6|> 98%
// 6-Bromoacetyl-1,4-benzodioxan|4629-54-3|>96%
// 6-Methyl-2-pyridinecarboxaldehyde|1122-72-1|>98%
// 6-Methyl-2-pyridinemethanol|1122-71-0|>98%
// 6-n-Butyltetralin|30654-45-6|>98%
// 7-Bromo-3,4-dihydro-1,5-benzodioxepin|147644-11-9|>97%
// 9,10-Dibromo-2-methylanthracene|177839-45-1|>98%
// 9,10-Dibromo-2-pentylanthracene|Not found.|>98%
// 9,10-Dibromo-2-phenylanthracene|929103-26-4|>99%
// 9,9-Bis-(3-bromophenyl)fluorene|Not found.| >98%
// 9-(3-Bromophenyl)-9-phenylfluorene|1257251-75-4|
// 9-Acetylanthracene|784-04-3|>96 %
// 9-Bromo-10-(2-naphthyl)anthracene|474688-73-8|>99%
// 9-Bromo-10-phenylanthracene|23674-20-6|>99%
// Acetylprehnitene|34764-71-1|>98%
// Azulene|275-51-4| > 99%
// Benzoylferrocene| 1272-44-2|> 98%
// Benz{h}quinolin-5,6-dione|65938-98-9| >98 %
// BP methyl ester| 330649-80-4|>95%
// Cholestenone|601-57-0|
// cis-Stilbene|645-49-8|> 97%
// Cyclohexanone cyanohydrin|931-97-5|>98%
// Cyclopentylacetone|1122-98-1 |> 98% (GC)
// Dibromoprehnitene| 36321-73-0| > 98 %
// Diethyl 2,5-dibromoadipate|869-10-3| 98+ %
// Dimethyl 2,5-dibromoterephthalate|18014-00-1|>98 %
// Ethyl 1-propenyl ether|928-55-2|>98%
// Ethyl 2-cyano-4,4-diethoxybutanoate|52133-67-2|>98% (GC)
// Ethyl 2-methylnicotinate|1721-26-2|>98%
// Fluorescamine|38183-12-9|>99% (HPLC)
// Hexamethylbenzene|87-85-4|99%
// N,N-Dihexylaniline|4430-09-5|>99%
// N,N-Dimethyl-4-tert-butylaniline|2909-79-7|>98%
// N,N-Dioctylaniline|3007-75-8| >98% (GC)
// n-Hexylbenzene|1077-16-3|> 98%
// n-Octylbenzene|2189-60-8|>99%
// o-Terphenyl|84-15-1|>99 %
// p-Quaterphenyl|135-70-6|>99 %
// p-Quinquephenyl|3073-05-0|>98 %
// p-Sexiphenyl|4499-83-6| >98%
// Pentylbenzene|538-68-1|>99 %
// Phenyl-p-benzoquinone|363-03-1|>99%
// Phenylsulfonylacetonitrile|7605-28-9| >98% (GC)
// Piperazine-2-one|5625-67-2|>98%
// Propylbenzoylpropionic acid|57821-78-0|>99%
// Quinoline-4-carboxylic acid|486-74-8|>97 %
// Sodium 3-carbazolesulfonate|17352-62-4|>98% (HPLC)
// Tolane 1T2|22692-80-4|>99%
// Tolane 1T3|184161-94-2|>99.5%
// Tolane 1T6| 117923-35-0|>99.5%
// Tolane 2T2|79135-69-6|>99%
// Tolane 2T3|102225-55-8|>99.5%
// Tolane 2T6|117923-34-9|>99.5%
// Tolane 2TO1|63221-88-5|>99%
// Tolane 3TO1|39969-26-1|>99.5 %
// Tolane 3TOH|107394-89-8|>98%
// Tolane 4T4|80221-11-0|>99.5%
// Tolane 4TO1|35684-12-9|>99%
// Tolane 4TO2|85583-83-1|>99.5%
// Tolane 5TO1|39969-28-3|>99 %
// Tolane 5TO2|95480-29-8|>99%
// Tolane|501-65-5|>99 %
// trans,trans-Muconic acid|3588-17-8|>98%
// Tri-n-butylcarbinol| 597-93-3| > 98%
// Trimethylacetonitrile|630-18-2|>98%";
//
//         $lines = explode("\n", $filedata);
//
//         foreach($lines as $line){
//             $attrs = explode("|", $line);
//
//             DB::table('products')->insertGetId([
//                 'name' => $attrs[0],
//                 'CAS' => $attrs[1],
//                 'purity' => $attrs[2],
//             ]);
//         }
//
//
//
//     }


    public function run()
    {
        $filedata = "(3,5-Diphenylphenyl)boronic acid@(3,5-Diphenylphenyl) boronic acid@128388-54-5@98%@5 g - 140€|10 g - 240€|25 g - 550€
1,1,3,3-Tetraethoxy-2-methylpropane@1,1,3,3-Tetraethoxy-2-methylpropane@10602-37-6@98%@
1,1'-Binaphthyl@1,1'-Binaphthalene@604-53-5@98%@5 g - 150€|10 g - 260€|25 g - 560€
1,3-Dibromo-5-hexylbenzene@1,3-Dibromo-5-hexylbenzene@75894-97-2@98%@50 g - 200€|100 g - 350€|250 g - 790€
1,4-Benzodioxane@1,4-Benzodioxane@493-09-4@99%@250 g - 100€|500 g - 180€|1000 g - 320€
1,4-Dicyanonaphthalene@Naphthalene-1,4-dicarbonitrile@3029-30-9@98%@
1,4-Dimethoxynaphthalene@1,4-Dimethoxynaphthalene@10075-62-4@99%@25 g - 330€|50 g - 590€|100 g - 1050€
1-Bromo-3,5-di-hexyl-benzene@1-Bromo-3,5-di-n-hexylbenzene@22385-77-9@98%@
1-Bromo-3-hexylbenzene@1-Bromo-3-n-hexylbenzene@38409-59-5@97%@
1-Bromo-4-n-hexylbenzene@1-Bromo-4-n-hexylbenzene@23703-22-2@99%@100 g - 140€|250 g - 300€|500 g - 520€
1-Bromo-4-n-octylbenzene@1-Bromo-4-n-octylbenzene@51554-93-9@98%@500 g - 1100€|1000 g - 1900€
1-Bromo-4-nonylbenzene@1-Bromo-4-nonylbenzene@51554-94-0@98%@100 g - 300€|250 g - 680€|500 g - 1210€
1-Ethynyl-4-fluorobenzene@4-Fluorophenylacetylene@766-98-3@99%@250 g - 650€|500 g - 1160€
1-Ethynyl-4-methoxybenzene@4-Methoxyphenylacetylene@768-60-5@99%@50 g - 145€|100 g - 250€
1-Iodonaphthalene@1-Iodonaphthalene@90-14-2@99%@250 g - 200€|500 g - 360€
1-Methyl-1H-pyrazole-4-sulfonyl chloride@1-Methyl-1H-imidazole-4-sulfonyl chloride@137049-00-4@98%@500 g - 170€|1000 g - 300€
2-(4-Ethylphenyl)-5-propylpyrimidine@2-(4-Ethylphenyl)-5-n-propylpyrimidine@98495-11-5@99,90%@250 g - 400€|500 g - 710€|1000 g - 1250€
2-(p-Tolyl)pyridine@2-(p-Tolyl)pyridine@4467-06-05@98%@50 g - 180€|100 g - 320€|250 g - 720€
2,2,4,7-Tetramethyl-1,2,3,4-tetrahydroquinoline@1,2,3,4-Tetrahydro-2,2,4,7-tetramethylquinoline@59388-58-8@97%@100 g - 150€
2,2'-Bicinchoninic acid@2,2?-Biquinoline-4,4?-dicarboxylic acid@1245-13-2@98%@5 g - 150€|10 g - 260€|25 g - 560€
2,2'-Bipyridine-4,4'-dicarboxylic acid@4,4'-Dicarboxy-2,2'-bipyridine@6813-38-3@99%@10 g - 100€|100 g - 700€|250 g - 1575€|500 g - 2800€
2,2'-Dibromobiphenyl@2,2'-Dibromobiphenyl@13029-09-9@98%@50 g - 320€|100 g - 560€|250 g - 1270€
2,3,5-Triphenylpyrazine@2,3,5-Triphenylpyrazine@36476-77-4@98%@10 g - 200€|25 g - 450€|50 g - 800€
2,3-Dimethylnaphthalene@2,3-Dimethylnaphthalene@581-40-8@98%@1g - 100€
2,4-Dichloro-6-phenylthieno[3,2-d]pyrimidine@2,4-Dichloro-6-phenylthieno[3,2-d]pyrimidine@36926-41-7@98%@5 g - 130€|10 g - 220€|25 g - 500€
2,4-Dimethylquinoline@2,4-Dimethylquinoline@1198-37-4@98%@50 g - 300€|100 g - 530€|250 g - 1200€
2,4-Ethen-di-yl-bipyridine@2,4'-(Ethene1,2-diyl)dipyridine@14802-41-6@98%@
2,5-Dibromo-para-xylene@1,4-Dibromo-2,5-dimethylbenzene@1074-24-4@98%@
2,6-Di(4-methylphenyl)pyridine@2,6-Bis(p-tolyl)pyridine@14435-88-2@98%@25 g - 210€|50 g - 370€|100 g - 650€
2,6-Dimethyl-4-tert-butylaniline@4-(tert-Butyl)-2,6-dimethylaniline@42014-60-8@99%@25 g - 200€|50 g - 350€|100 g - 620€
2,6-Dimethyl-4-tert-butylbenzoyl chloride@2,6-Dimethyl-4-tert-butylbenzoyl chloride@119335-81-8@98%@50 g - 220€|100 g - 380€|250 g - 850€
2,6-Diphenylpyridine@2,6-Diphenylpyridine@3558-69-8@97%@
2,6-Di-tert-butyl-1,4-benzoquinone@2,6-Di-tert-butyl-1,4-benzoquinone@719-22-2@98%@
2,6-Di-tert-butylpyridine@2,6-Di-tert-butylpyridine@585-48-8@98%@500 g - 915€|1000 g - 1600€|3000 g - 4200€
2,6-Pyridinedicarbonyl dichloride@2,6-Pyridinedicarboxylic acid chloride@3739-94-4@98%@50 g - 100€|100 g - 180€
2-Chloro-5-ethylpyrimidine@2-Chloro-5-ethylpyrimidine@111196-81-7@98%@5 g - 100€|10 g - 180€
2-Chloro-5-n-pentylpyrimidine@2-Chloro-5-n-pentylpyrimidine@154466-62-3@98%@1g - 100€|2,5 g - 220€|5 g - 390€
2-Chlorolepidine@2-Chloro-4-methylquinoline@634-47-9@99%@10 g - 25€|25 g - 57€|50 g - 101€
2-Iodobiphenyl@2-Iodobiphenyl@2113-51-1@98%@500 g - 315€|1000 g - 550€
2'-Iodo-m-terphenyl@2'-Iodo-1,1':3',1''-terphenyl@82777-09-1@99%@10 g - 330€|25 g - 750€|50 g - 1350€
2-Mercapto-5-propylpyrimidine@2-Mercapto-5-n-propylpyrimidine@52767-84-7@98%@5 g - 150€|10 g - 260€|25 g - 560€
2-Phenylanthracene@2-Phenylanthracene@1981-38-0@99%@25 g - 140€|50 g - 320€|100 g - 570€
2-Phenylanthraquinone@2-Phenylanthraquinone@6485-97-8@98%@10 g - 100€|25 g - 230€
2-Phenylpyridine@2-Phenylpyridine@1008-89-5@98%@250 g - 390€|500 g - 700€
2-tert-Butylanthracene@2-(tert-Butyl)anthracene@18801-00-8@98%@50 g - 285€|100 g - 500€|250 g - 1130€
3-(Bromomethyl)biphenyl@3-(Bromomethyl)biphenyl@14704-31-5@98%@50 g - 120€|100 g - 200€
3,3'-Dibromobenzophenone@3,3'-Dibromobenzophenone@25032-74-0@98%@
3,4,5,6-Tetrachloro-1,2-benzoquinone@Tetrachloro-1,2-benzoquinone@2435-53-2@98%@
3,5-Dibromobiphenyl@3,5-Dibromo-1-phenyl benzene@16372-96-6@99%@1g - 150€|2,5 g - 340€|5 g - 605€
3,5-Di-tert-butyl-1,2-benzoquinone@3,5-Di-tert-butyl-o-benzoquinone@3383-21-9@98%@50 g - 150€|100 g - 260€|250 g - 580€
3,5-Di-tert-butyl-4-hydroxybenzaldehyde@3,5-di-tert-butyl-4-hydroxybenzaldehyde@1620-98-0@98%@50 g - 100€|100 g - 180€
3,5-Di-tert-butyl-4-hydroxyphenylacetonitrile@3,5-Di-tert-butyl-4-hydroxyphenylacetonitrile@1611-07-0@98%@50 g - 320€|100 g - 560€|250 g - 1260€
3-Bromo-2-ethoxypyridine@3-Bromo-2-ethoxypyridine@57883-25-7@97%@2,5 g - 160€|5 g - 290€|10 g - 500€
3-Bromo-4-ethylphenol@3-Bromo-4-ethylphenol@540495-28-1@98%@100 g - 210€|250 g - 470€|500 g - 830€
3-Bromobenzophenone@3-Bromobenzophenone@1016-77-9@98%@25 g - 2500€|50 g - 4450€|100 g - 7780€
3-Bromodibenzofuran@3-Bromodibenzofuran@26608-06-0@99,50%@10 g - 200€|25 g - 450€|50 g - 800€
3-Ethylbenzoic acid@3-Ethylbenzoic acid@619-20-5@98%@
3-Ethylbiphenyl@3-Ethyl-1,1'-biphenyl@5668-93-9@99%@
3-Methylphenyl-phenylfluorenyl-amine@3-Methylphenyl-3-(9-phenyl-9H-fluoren-9-yl)phenylamine@1292285-22-3@98%@250 g - 1350€|500 g - 2400€|1000 g - 4200€
3-Nitrostyrene@3-Nitrostyrene@586-39-0@97%@
4-(3-Chlorophenyl)pyridine@4-(3-Chlorophenyl)pyridine@5957-92-6@98%@2,5 g - 100€|5 g - 180€|10 g - 315€
4-(4-Pyridyl)benzoic acid@4-(4-Pyridyl)benzoic acid@4385-76-6@98%@
4-(Bromomethyl)biphenyl@4-(Bromomethyl)biphenyl@2567-29-5@97%@
4,4'-Diacetylbiphenyl@4,4'-Diacetylbiphenyl@787-69-9@98%@50 g - 260€|100 g - 460€
4,4'-Dibromobenzophenone@4,4?-Dibromobenzophenone@3988-03-02@98%@50 g - 100€|100 g - 170€
4,4-Dibromo-p-terphenyl-Specification@4,4??-Dibromo-p-terphenyl@17788-94-2@99,80%@25 g - 140€|50 g - 250€
4,4'-Dibromotolane@1,1’-(1,2-Ethynediyl)-bis(4-bromo)benzene@2789-89-1@99%@100 g - 260€|250 g - 590€
4,4'-Diiodo-3,3'-dimethylbiphenyl@4,4'-Diiodo-3,3'-dimethylbiphenyl@7583-27-9@98%@
4,4'-di-tert-Butylbiphenyl@4,4'-Di-tert-Butylbiphenyl@1625-91-8@99%@
4-Aminobenzoylhydrazine@4-Aminobenzoic hydrazide@5351-17-7@98%@250 g - 130€
4-Bromo-2-nitrophenol@4-Bromo-2-nitrophenol@7693-52-9@98%@
4'-Bromobiphenyl-4-amine@4-Amino-4'-bromobiphenyl@3365-82-0@99%@50 g - 260€|100 g - 450€|250 g - 1010€
4-Bromo-N,N-dihexylaniline@4-Bromo-N,N-dihexylaniline@425604-49-5@98%@50 g - 110€
4-Bromostyrene - Spec@4-Bromostyrene@2039-82-9@98%@
4-Bromostyrene oxide@4-Bromostyrene oxide@32017-76-8@98%@
4-Chlorostyrene oxide@4-Chlorostyrene oxide@2788-86-5@98%@
4-Cyano-4'-n-decylbiphenyl@4-Cyano-4'-n-decylbiphenyl@59454-35-2@99%@100 g - 1200€
4-Cyano-4'-nonylbiphenyl@4-Cyano-4'-nonylbiphenyl@52709-85-0@99%@5 g - 150€
4-Cyano-4'-undecylbiphenyl@4-Cyano-4'-n-undecylbiphenyl@65860-74-4@99%@5 g - 150€
4-Cyclohexylbenzoic acid@4-Cyclohexylbenzoic acid@20029-52-1@98%@50 g - 570€|100 g - 1000€
4-Decyloxy-4'-cyanobiphenyl@4'-Cyano-4-decyloxybiphenyl@70247-25-5@99%@10 g - 140€|25 g - 310€
4-Ethynyl-2,6-difluoroaniline@4-Ethynyl-2,6-difluoroaniline@753501-37-0@99%@250 g - 1690€|500 g - 3000€
4-Ethynyltoluene@4-Ethynyltoluene@766-97-2@98%@
4-Fluorostyrene@4-Fluorostyrene@405-99-2@97%@250 g - 485€|500 g - 860€
4-Heptylbenzoic acid@4-Heptylbenzoic acid@38350-87-7@97%@50 g - 115€|100 g - 200€
4-n-Butylphenylacetylene@1-Butyl-4-ethynylbenzene@79887-09-5@98%@250 g - 230€|500 g - 400€|1000 g - 700€
4-n-Decylaniline@4-Decylaniline@37529-30-9@98%@50 g - 180€|100 g - 320€
4-n-Hexylaniline@4-n-Hexylaniline@33228-45-4@98%@500 g - 745€|1000 g - 1300€
4-Nitrophenylacetone@4-Nitrophenylacetone@5332-96-7@98%@25 g - 230€|50 g - 400€|100 g - 700€
4-n-Octylbenzoic acid@4-n-Octylbenzoic acid@3575-31-3@98%@25 g - 110€|50 g - 200€|100 g - 345€
4-n-Octylphenol@4-n-Octylphenol@1806-26-4@99%@50 g - 200€|100 g - 345€|250 g - 780€
4-n-Pentylaniline@4-n-Pentylaniline@33228-44-3@98%@10 g - 150€|25 g - 340€
4-Octyl-4'-cyanobiphenyl@4'-n-Octylbiphenyl-4-carbonitrile@52709-84-9@99%@50 g - 200€|100 g - 350€|250 g - 780€
4-Phenylpyridine@4-Phenylpyridine@939-23-1@99%@50 g - 100€|100 g - 170€|250 g - 390€
5,5-Dimethyl-2,2-bipyridine@5,5'-Dimethyl-2,2'-bipyridyl@1762-34-1@98%@25 g - 100€|50 g - 170€|100 g - 290€
5,9-Dibromo-7,7-dimethyl-7H-benzofluorene@5,9-Dibromo-7,7-dimethyl-7H-benzo(c)fluorene@1056884-35-5@99%@25 g - 215€|50 g - 380€|100 g - 670€
5-Bromo-2,2'-bipyridine@5-Bromo-2,2'-bipiridinyl@15862-19-8@98%@
5-Hexen-2-one@5-Hexen-2-one@109-49-9@98%@
5-Methyl-2,2'-bipyridine@5-Methyl-2,2'-bipyridyl@56100-20-0@98%@
5-Phenylpentan-1-ol@5-Phenylpentan-1-ol@10521-91-2@98%@500 g - 1000€|1000 g - 1750€
6-Bromo-1,4-benzodioxane@6-Bromo-1,4-benzodioxane@52287-51-1@98%@50 g - 110€|100 g - 190€
6-Bromo-2,2'-bipyridine@6-Bromo-2,2'-bipyridine@10495-73-5@98%@
6-Methyl-2-pyridinecarboxaldehyde@6-Methyl-2-pyridinecarboxaldehyde@1122-72-1@98%@25 g - 90€|50 g - 160€|100 g - 280€
6-Methyl-2-pyridinemethanol@6-Methyl-2-pyridinemethanol@1122-71-0@98%@
9,10-Dibromo-2-methylanthracene@9,10-Dibromo-2-methylanthracene@177839-45-1@98%@10 g - 135€|25 g - 300€|50 g - 530€
9-Acetylanthracene@9-Acetylanthracene@784-04-3@96%@25 g - 70€|50 g - 130€|100 g - 225€
Azulene@Azulene@275-51-4@99%@
cis-Stilbene@cis-Stilbene@645-49-8@97%@
Ethyl 1-propenyl ether@Ethyl propenyl ether,cis+trans@928-55-2@98%@
Fluorescamine@Fluorescamine@38183-12-9@99%@100 g - 2600€|250 g - 5850€|500 g - 10400€
Hexamethylbenzene@Hexamethylbenzene@87-85-4@99%@
N,N-Dihexylaniline@N,N-Di-n-hexylaniline@4430-09-05@99%@50 g - 215€|100 g - 375€|250 g - 835€
n-Hexylbenzene@n-Hexylbenzene@1077-16-3@98%@
n-Octylbenzene@n-Octylbenzene@2189-60-8@99%@250 g - 200€|500 g - 350€
Phenyl-p-benzoquinone@Phenyl-p-benzoquinone@363-03-1@99%@
Tolane@Diphenylacetylene@501-65-5@98%@
Tolane 1T2@4-Ethyl-4'-methyldiphenyl acetylene@22692-80-4@99%@50 g - 150€|100 g - 270€|250 g - 600€
Tolane 2T6@4-Ethyl-4'-hexyldiphenyl acetylene@117923-34-9@99,50%@100 g - 300€|250 g - 675€|500 g - 1200€
Tolane 2TO1@4-Ethyl-4'-methoxydiphenyl acetylene@63221-88-5@99%@250 g - 675€|500 g - 1200€|1000 g - 2100€
Tolane 3TO1@4-Propyl-4'-methoxydiphenyl acetylene@39969-26-1@99,50%@50 g - 170€|100 g - 300€|250 g - 675€
Tolane 4TO1@4-Butyl-4'-methoxydiphenyl acetylene@35684-12-9@99%@
Tolane 4TO2@4-Butyl-4'-ethoxydiphenyl acetylene@85583-83-1@99,50%@
Tolane 5TO1@4-Pentyl-4'-methoxydiphenyl acetylene@39969-28-3@99%@100 g - 300€|250 g - 675€|500 g - 1200€
Trimethylacetonitrile@Trimethylacetonitrile@630-18-2@98%@";


        $lines = explode("\n", $filedata);

        $successfulSeed = 0;
        $iter = 0;
        foreach($lines as $line)
        {
            $attributes = explode("@", $line); // 0 - filename, 1 - product name, 2 - cas, 3 - purity, 4 - prices

            $prices = explode("|", $attributes[4]);

            $successfulSeed += $this->SeedProduct($attributes[0], // filename
                                                    $attributes[1], // product name
                                                    $attributes[2], // cas
                                                    $attributes[3], // purity
                                                    $prices); // prices


            $iter++;
            //if($iter == 24) break;
        }

        print("\n\n!!!!!!!!!   Products: " . $iter . "    Successful: " . $successfulSeed . "   !!!!!!!!!!!!");
    }

    private function SeedProduct($fileName, $productName, $cas, $purity, $prices)
    {
        if(!file_exists($this->descriptionPath . $fileName . ".htm") ||
           !file_exists($this->imgPath . $fileName . ".png"))
        {
            return 0;
        }

        print($fileName . ".htm");

        $newProductId = $this->CreateProductInDB($productName, $cas, $purity);

        $this->ModifyHTML($fileName, $newProductId);

        $this->AddPrices($newProductId, $prices);

        return 1;
    }

    private function CreateProductInDB($productName, $cas, $purity)
    {
        $productId = DB::table('products')->insertGetId([
            'name' => $productName,
            'CAS' => $cas,
            'purity' => $purity,
        ]);

        return $productId;
    }

    private function ModifyHTML($fileName, $id)
    {
        rename($this->descriptionPath . $fileName . '.htm', $this->descriptionPath . $id . '.htm');
        rename($this->imgPath . $fileName . '.png', $this->imgPath . $id . '.png');

        $this->productPageModifier->Modify($this->descriptionPath . $id . '.htm', $this->appImgPath . $id . '.png');
    }

    private function AddPrices($productId, $prices)
    {
        $product = \App\Product::find($productId);

        foreach($prices as $price)
        {
            print("Price: " . $price);

            if(strlen($price) == 0)
            {
                print("  denied");
            }
            else
            {
                $product->prices()->create(['price' => $price]);
            }

            print("\n");
        }
    }
}
