<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Info::class, function (Faker\Generator $faker) {
    return [
        'type' => 'contact',
        'name' => $faker->regexify('[A-Z]{10}'),
        'body' => $faker->unique()->regexify('[A-Z]{10}')
    ];
});


$factory->define(\App\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->regexify('[A-Z]{10}'),
        'purity' => (string)$faker->randomDigit,
        'CAS' => $faker->unique()->regexify('[0-9]{10}')
    ];
});

$factory->define(\App\CompClass::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->regexify('[A-Z]{10}')
    ];
});

$factory->define(\App\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->unique()->regexify('[A-Z]{20}'),
        'body' => $faker->realText($maxNbChars = 350, $indexSize = 2),
        'image' => 'none'
    ];
});
