<?php

View::composer('layouts.master', 'App\Composers\MasterViewComposer');

// Main routes
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');


// Contacts
Route::get('/contacts', 'ContactsController@index');
Route::get('/updatecontacts', 'ContactsController@edit');
Route::post('/updatecontacts', 'ContactsController@update');
Route::post('/sendmail', 'ContactsController@sendMail');

// About
Route::get('/about', 'AboutController@index');
Route::get('/updateabout', 'AboutController@edit');
Route::post('/updateabout', 'AboutController@update');

// Slide text
Route::get('/updateslidetexts', 'SlideTextController@edit');
Route::post('/updateslidetexts', 'SlideTextController@update');


// Auth
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('/compclasses/list', 'ClassesController@list');
Route::get('/compclasses/links', 'ClassesController@links');
Route::resource('compclasses', 'ClassesController');

Route::get('/news/list', 'NewsController@list');
Route::resource('news', 'NewsController');

Route::get('/products/list', 'ProductsController@list');
Route::get('/products/class/{name}', 'ProductsController@showClass');
Route::get('/searchresults', 'ProductsController@showSearch'); //----------------------------
Route::resource('products', 'ProductsController');
