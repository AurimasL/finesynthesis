<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminListPageTest extends TestCase
{
    use DatabaseTransactions;

    private $user = null;

    public function setUp()
    {
        parent::setUp();

        if($this->user == null){
            $this->user = factory(\App\User::class)->create(); // Create dummmy user
        }
    }

    public function test_shows_list_of_products()
    {
        printf(__METHOD__ . "\n");

        $compounds = factory(\App\Product::class, 10)->create();

        $response = $this->actingAs($this->user)->get('/products/list');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($compounds as $compound){
            $response->assertSee($compound->name);
        }
    }

    public function test_shows_list_of_classes()
    {
        printf(__METHOD__ . "\n");

        $compClasses = factory(\App\CompClass::class, 5)->create();

        $response = $this->actingAs($this->user)->get('/compclasses/list');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($compClasses as $compClass){
            $response->assertSee($compClass->name);
        }
    }

    public function test_shows_list_of_news()
    {
        printf(__METHOD__ . "\n");

        $posts = factory(\App\Post::class, 5)->create();

        $response = $this->actingAs($this->user)->get('/news/list');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($posts as $post){
            $response->assertSee($post->title);
        }
    }
}
