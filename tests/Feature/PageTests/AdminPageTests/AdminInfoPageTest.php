<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminInfoPageTest extends TestCase
{
    use DatabaseTransactions;

    private $user = null;

    public function setUp()
    {
        parent::setUp();

        if($this->user == null){
            $this->user = factory(\App\User::class)->create(); // Create dummmy user
        }
    }

    public function test_shows_about_paragraph_edit_fields()
    {
        printf(__METHOD__ . "\n");

        $aboutpars = factory(\App\Info::class, 5)->create(['type' => 'about']);

        $response = $this->actingAs($this->user)->get('/updateabout');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($aboutpars as $aboutpar){
            $response->assertSee($aboutpar->name);
            $response->assertSee($aboutpar->body);
        }
    }

    public function test_shows_contacts_edit_fields()
    {
        printf(__METHOD__ . "\n");

        $contacts = factory(\App\Info::class, 6)->create();

        $response = $this->actingAs($this->user)->get('/updatecontacts');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($contacts as $contact){
            $response->assertSee($contact->name);
            $response->assertSee($contact->body);
        }
    }
}
