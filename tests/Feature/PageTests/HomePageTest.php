<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomePageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_main_components_present()
    {
        printf(__METHOD__ . "\n");

        $response = $this->get('/');

        $response->assertStatus(200) // Page is loading, no login required
                 ->assertSee('News')      //
                 ->assertSee('About us')  //  Applies to all views
                 ->assertSee('Contacts')  //
                 ->assertSee('Search');
    }

    public function test_two_contacts_present_in_footer()
    {
        printf(__METHOD__ . "\n");
        
        $contacts = factory(\App\Info::class, 3)->create();

        $response = $this->get('/');

        $response->assertStatus(200) // Page is loading, no login required
                 ->assertSee($contacts[0]->name . ': ' . $contacts[0]->body)  // Page loads two first contacts from DB
                 ->assertSee($contacts[1]->name . ': ' . $contacts[1]->body);
    }
}
