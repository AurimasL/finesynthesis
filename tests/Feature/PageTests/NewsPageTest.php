<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NewsPageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_shows_news()
    {
        printf(__METHOD__ . "\n");

        $news = factory(\App\Post::class, 3)->create();

        $response = $this->get('/news');

        $response->assertStatus(200) // Page is loading, no login required
                 ->assertSee($news[0]->title)  // Page loads all paragraphs
                 ->assertSee($news[0]->body)
                 ->assertSee($news[1]->title)
                 ->assertSee($news[1]->body)
                 ->assertSee($news[2]->title)
                 ->assertSee($news[2]->body);
    }
}
