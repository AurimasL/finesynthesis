<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactsPageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_shows_contacts()
    {
        printf(__METHOD__ . "\n");
        
        $contacts = factory(\App\Info::class, 5)->create();

        $response = $this->get('/contacts');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($contacts as $contact){
            $response->assertSee($contact->name);  // Page loads all paragraphs
            $response->assertSee($contact->body);
        }
    }
}
