<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClassesPageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_shows_classes()
    {
        printf(__METHOD__ . "\n");

        $compClasses = factory(\App\CompClass::class, 5)->create();

        $response = $this->get('/compclasses');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($compClasses as $compClass){ // Page loads all paragraphs
            $response->assertSee($compClass->name);
        }
    }
}
