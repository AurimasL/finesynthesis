<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductsPageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_shows_products()
    {
        printf(__METHOD__ . "\n");

        $compounds = factory(\App\Product::class, 5)->create();

        $response = $this->get('/products');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($compounds as $compound){
            $response->assertSee($compound->name)  // Page loads all paragraphs
                     ->assertSee($compound->purity)
                     ->assertSee($compound->CAS);
        }
    }

    public function test_shows_classes_in_products_page()
    {
        printf(__METHOD__ . "\n");

        $compClasses = factory(\App\CompClass::class, 5)->create();

        $response = $this->get('/products');

        $response->assertStatus(200); // Page is loading, no login required

        foreach($compClasses as $compClass){ // Page loads all paragraphs
            $response->assertSee($compClass->name);
        }
    }
}
