<?php

namespace Tests\Feature\PageTests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AboutPageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_shows_about_paragraphs()
    {
        printf(__METHOD__ . "\n");
        
        $aboutpars = factory(\App\Info::class, 3)->create(['type' => 'about']);

        $response = $this->get('/about');

        $response->assertStatus(200) // Page is loading, no login required
                 ->assertSee($aboutpars[0]->name)  // Page loads all paragraphs
                 ->assertSee($aboutpars[0]->body)
                 ->assertSee($aboutpars[1]->name)
                 ->assertSee($aboutpars[1]->body)
                 ->assertSee($aboutpars[2]->name)
                 ->assertSee($aboutpars[2]->body);
    }
}
