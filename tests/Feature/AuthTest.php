<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    private $user = null;

    public function setUp()
    {
        parent::setUp();

        if($this->user == null){
            $this->user = factory(\App\User::class)->create(); // Create dummmy user
        }
    }

    private $adminRoutes = [
        '/products/list',
        '/compclasses/list',
        '/news/list',
        '/news/create',
        '/news/1/edit', // Id = 1, faker not needed, because single view is used for new posts and for editing
        '/compclasses/create',
        '/compclasses/1/edit', // Id = 1, faker not needed, because single view is used for new posts and for editing
        '/products/create',
        '/updatecontacts',
        '/updateabout'
    ];



    public function test_admin_routes_require_login()
    {
        $compound = factory(\App\Product::class)->create();
        $compclass = factory(\App\CompClass::class)->create();
        $news = factory(\App\Post::class)->create();

        foreach($this->adminRoutes as $adminRoute){
            $response = $this->get($adminRoute);

             print($adminRoute);
             print($response->getStatusCode());
             print("\n");

            $response->assertStatus(302) // Redirected to login page
                     ->assertSee('Redirecting to') // See intermediate redirecting response
                     ->assertSee('login'); // Which contains login word
        }

        $this->actingAs($this->user);

        foreach($this->adminRoutes as $adminRoute){
            $response = $this->get($adminRoute);

            print($adminRoute);
            print($response->getStatusCode());
            print("\n");

            $response->assertStatus(200); // Visit after log in
        }
    }

    // Tests for routes that need entries in DB
    public function test_product_admin_route_requires_login(){
        $compound = factory(\App\Product::class)->create();

        $this->get('/products/' . $compound->id . '/edit')->assertStatus(302) // Redirected to login page
                                                   ->assertSee('Redirecting to') // See intermediate redirecting response
                                                   ->assertSee('login'); // Which contains login word

        $this->actingAs($this->user)->get('/products/' . $compound->id . '/edit')->assertStatus(200); // Visit after log in
    }


    // public function testRegisterRoutes()
    // {
    //     $this->get('/register')->assertStatus(404); // In production version there should be register form,
    //                                                 // so that guests could not gain access to admin panel.
    // }
}
