<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomeTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_search()
    {
        $product = factory(\App\Product::class)->create();

        $this->browse(function (Browser $browser) use ($product) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/')
                    ->type('query', $product->name)
                    ->press('Search')
                    ->assertSee($product->name);
        });

        $product->delete();
    }
}
