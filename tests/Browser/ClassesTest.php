<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClassesTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_create_new_class()
    {
        $classname = 'Carbohydrates';

        $this->browse(function (Browser $browser) use ($classname) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/compclasses/list') // Open list
                    ->assertSee('List of compclasses')
                    ->clickLink('Create new') // Test Create new link
                    ->assertSee('class')
                    ->type('name', $classname) // Create new class
                    ->press('Save');
        });

        $classone = \App\CompClass::where('name', $classname)->first();

        $this->assertTrue($classone->name == $classname);

        $classone->delete();
    }

    public function test_edit_class(){
        $classname = 'Carbohydrates';
        $newclassname = 'Polyols';

        $classmodel = \App\CompClass::create(['name' => $classname]);
        $classmodel->save();

        $this->browse(function (Browser $browser) use ($classname, $classmodel, $newclassname) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('compclasses/' . $classmodel->id . '/edit') // Open list
                    ->assertSee('Class')
                    ->assertInputValue('name', $classname)
                    ->type('name', $newclassname) // Create new class
                    ->press('Save');
        });

        $this->assertTrue(\App\CompClass::find($classmodel->id)->name == $newclassname);

        $classmodel->delete();
    }

    public function test_delete_class(){
        $classname = 'Carbohydrates';

        $classmodel = \App\CompClass::create(['name' => $classname]);
        $classmodel->save();

        $this->browse(function (Browser $browser) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/compclasses/list') // Open list
                    ->assertSee('List of compclasses')
                    ->clickLink('Delete');
        });

        $getclass = \App\CompClass::find($classmodel->id);

        $this->assertFalse(isset($getclass->name));
    }
}
