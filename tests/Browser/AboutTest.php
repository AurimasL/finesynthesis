<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class AboutTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_about_edit_page()
    {
        $aboutpar = factory(\App\Info::class, 2)->create(['type' => 'about']);

        $this->browse(function (Browser $browser) use ($aboutpar) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/updateabout') // Open list
                    ->assertSee('Update About')
                    ->type('parnames[0]', $aboutpar[0]->name) // Create new class
                    ->type('parbodies[0]', $aboutpar[0]->body)
                    ->type('parnames[1]', $aboutpar[1]->name) // Create new class
                    ->type('parbodies[1]', $aboutpar[1]->body)
                    ->press('Save')
                    ->visit('/updateabout')
                    ->assertInputValue('parnames[0]', $aboutpar[0]->name)
                    ->assertInputValue('parbodies[0]', $aboutpar[0]->body)
                    ->assertInputValue('parnames[1]', $aboutpar[1]->name)
                    ->assertInputValue('parbodies[1]', $aboutpar[1]->body);
        });

        DB::table('infos')->delete();
    }
}
