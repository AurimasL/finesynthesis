<?php

namespace Tests\Browser\Products;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class ProductsTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_create_product()
    {
        $product = ['name' => 'hexane', 'cas' => '5874', 'purity' => '95'];

        $classes = factory(\App\CompClass::class, 2)->create();

        $this->browse(function (Browser $browser) use ($product, $classes) {
            $price1 = '1254598562';
            $price2 = '657898455';

            $browser->loginAs(\App\User::all()->first())
                    ->visit('/products/list') // Check if list works
                    ->assertSee('List of products')
                    ->clickLink('Create new')
                    ->assertSee('Upload Product')

                    ->assertPathIs('/products/create') // Create product link works
                    ->type('name', $product['name']) // Fill fields, add classes
                    ->type('cas', $product['cas'])
                    ->type('purity', $product['purity'])
                    ->check('compclasses[' . $classes[0]->id . ']')
                    ->type('prices[0]', $price1)
                    ->type('prices[1]', $price2)
                    ->assertChecked('compclasses[' . $classes[0]->id . ']') // test if fields were filed correctly, classes checked
                    ->assertInputValue('prices[0]', $price1)
                    ->assertInputValue('prices[1]', $price2)
                    ->press('Save') // Upload product

                    ->assertSee($product['name']) // In specific product page check if product name is displayed
                    ->assertSee($classes[0]->name) // Check if product belongs to asigned class

                    ->visit('/products') // Visit page of all products
                    ->assertSee($product['name'])
                    ->assertSee($product['cas'])
                    ->assertSee($product['purity'])
                    ->click('.glyphicon-triangle-bottom') // Reveal prices
                    ->pause(1000) // Wait for popup
                    ->assertSee('Prices:')
                    ->assertSee($price1)
                    ->assertSee($price2);
        });

        DB::table('products')->delete();
        DB::table('comp_classes')->delete();
        DB::table('comp_class_product')->delete();
        DB::table('prices')->delete();
    }

    public function test_edit_product()
    {
        $product = factory(\App\Product::class)->create();

        $classes = factory(\App\CompClass::class, 2)->create();

        $new_prod_values = ['name' => 'Octane', 'cas' => '41254', 'purity' => '52', 'price1' => 'five dollars', 'price2' => 'ten euros'];

        $this->browse(function (Browser $browser) use ($product, $classes, $new_prod_values) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/products/list') // Check if list works
                    ->assertSee('List of products')
                    ->clickLink('Edit')
                    ->assertSee('Upload Product')

                    ->assertPathIs('/products/' . $product->id . '/edit') // Edit product link works
                    ->assertInputValue('name', $product->name) // Check fields
                    ->assertInputValue('cas', $product->CAS)
                    ->assertInputValue('purity', $product->purity)

                    ->type('name', $new_prod_values['name']) // Modify
                    ->type('cas', $new_prod_values['cas'])
                    ->type('purity', $new_prod_values['purity'])
                    ->type('prices[0]', $new_prod_values['price1'])
                    ->type('prices[1]', $new_prod_values['price2'])
                    ->check('compclasses[' . $classes[0]->id . ']')
                    ->press('Save') // Update
                    ->assertPathIs('/products/' . $product->id . '/edit')

                    ->assertInputValue('name', $new_prod_values['name']) // Check fields
                    ->assertInputValue('cas', $new_prod_values['cas'])
                    ->assertInputValue('purity', $new_prod_values['purity'])
                    ->assertChecked('compclasses[' . $classes[0]->id . ']') // test if fields were filed correctly, classes checked
                    ->assertNotChecked('compclasses[' . $classes[1]->id . ']') // test if fields were filed correctly, classes checked
                    ->assertInputValue('prices[0]', $new_prod_values['price1'])
                    ->assertInputValue('prices[1]', $new_prod_values['price2']);
        });

        DB::table('products')->delete();
        DB::table('comp_classes')->delete();
        DB::table('comp_class_product')->delete();
        DB::table('prices')->delete();
    }

    public function test_delete_product()
    {
        $product = ['name' => 'hexane', 'cas' => '5874', 'purity' => '95', 'price1' => '58$', 'price2' => '854$'];

        $classes = factory(\App\CompClass::class, 2)->create();

        $this->browse(function (Browser $browser) use ($product, $classes) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/products/create') // Check if list works
                    ->type('name', $product['name']) // Fill fields, add classes
                    ->type('cas', $product['cas'])
                    ->type('purity', $product['purity'])
                    ->check('compclasses[' . $classes[0]->id . ']')
                    ->check('compclasses[' . $classes[1]->id . ']')
                    ->type('prices[0]', $product['price1'])
                    ->type('prices[1]', $product['price2'])
                    ->press('Save') // Upload product

                    ->visit('/products/list') // Check if list works
                    ->assertSee('List of products')
                    ->assertSee($product['name'])
                    ->clickLink('Delete')
                    ->assertSee('List of products')
                    ->assertMissing($product['name']);
        });

        DB::table('products')->delete();
        DB::table('comp_classes')->delete();

        // Check if no related data was left undeleted
        $this->assertTrue(DB::table('comp_class_product')->delete() == 0);
        $this->assertTrue(DB::table('prices')->delete() == 0);
    }
}
