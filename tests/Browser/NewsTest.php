<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NewsTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_create_news_post()
    {
        $news = ['title' => 'dasdfa4sdDFA5AF', 'body' => 'sdfer979er87ter89elak'];

        $this->browse(function (Browser $browser) use ($news) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/news/list')
                    ->assertSee('List of news')
                    ->clickLink('Create new')
                    ->assertSee('Post news')
                    ->type('title', $news['title'])
                    ->type('body', $news['body'])
                    ->press('Post')
                    ->visit('/news/list')
                    ->assertSee($news['title']);
        });

        \App\Post::first()->delete();
    }

    public function test_edit_post()
    {
        $news = factory(\App\Post::class)->create();

        $this->browse(function (Browser $browser) use ($news) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/news/list')
                    ->assertSee('List of news')
                    ->clickLink('Edit')
                    ->assertSee('Post news')
                    ->assertInputValue('title', $news->title)
                    ->assertInputValue('body', $news->body)
                    ->type('title', 'changed title') // Edit
                    ->type('body', 'changed body')
                    ->press('Post')
                    ->visit('/news/list')
                    ->clickLink('Edit')
                    ->assertInputValue('title', 'changed title')
                    ->assertInputValue('body', 'changed body');
        });

        $news->delete();
    }

    public function test_delete_post(){
        $news = factory(\App\Post::class)->create();

        $this->browse(function (Browser $browser) use ($news) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/news/list')
                    ->assertSee('List of news')
                    ->clickLink('Delete');
        });

        $getsamepost = \App\Post::find($news->id);

        $this->assertFalse(isset($getsamepost));
    }
}
