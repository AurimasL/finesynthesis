<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class ContactsTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_contacts_edit_page()
    {
        $contacts = factory(\App\Info::class, 2)->create();

        $this->browse(function (Browser $browser) use ($contacts) {
            $browser->loginAs(\App\User::all()->first())
                    ->visit('/updatecontacts') // Open list
                    ->assertSee('Update Contacts')
                    ->type('contactnames[0]', $contacts[0]->name) // Create new class
                    ->type('contactvalues[0]', $contacts[0]->body)
                    ->type('contactnames[1]', $contacts[1]->name) // Create new class
                    ->type('contactvalues[1]', $contacts[1]->body)
                    ->press('Save')
                    ->visit('/updatecontacts')
                    ->assertInputValue('contactnames[0]', $contacts[0]->name)
                    ->assertInputValue('contactvalues[0]', $contacts[0]->body)
                    ->assertInputValue('contactnames[1]', $contacts[1]->name)
                    ->assertInputValue('contactvalues[1]', $contacts[1]->body);
        });

        DB::table('infos')->delete();
    }
}
