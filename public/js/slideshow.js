$(function(){
    var slides = $(".mySlides");
    var dots = $(".dot");
    var numberOfSlides = slides.length;
    var i = 0;
    var nextSlideId = -1;
    var timeoutId = -1;
    var exposition = 7000;
    var halftrans = 1000;

    // Init
    slides[0].style.opacity = 1.0;
    slides.eq(0).addClass('active_slide');
    dots.eq(0).addClass('active_dot');

    var nextSlide = function(){
        if(nextSlideId != -1){
            timeoutId = setTimeout(changeSlide, exposition);
            console.log('new timeout: ' + timeoutId);
        }
        nextSlideId = -1; // After forced slide change executed, we can return to normal slide exposition
    }.bind(this);

    nextSlide();

    function changeSlide(){
        function fadeIn(){ // fade in
            slides.eq(i).removeClass('active_slide');
            dots.eq(i).removeClass('active_dot');
            if(nextSlideId != -1){
                i = nextSlideId;
            }else{
                i = i == numberOfSlides - 1 ? 0 : i + 1;
            }
            slides.eq(i).addClass('active_slide').animate({"opacity": 1.0}, halftrans, nextSlide);
            dots.eq(i).addClass('active_dot');
        }
        fadeIn.bind(this);

        slides.eq(i).animate({"opacity": 0.4}, halftrans, fadeIn); // fade out
    }

    $('.dot').click(function(){ // Forced slide change listener
        var dotId = $(this).index();
        if(dotId != i && nextSlideId == -1){
            slides.eq(i).stop();
            dots.eq(i).stop();
            clearTimeout(timeoutId);
            console.log('timeout: ' + timeoutId + ' cleared');
            nextSlideId = dotId;
            changeSlide();
        }
    });
});
