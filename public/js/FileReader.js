function ReadFile(fileInput, fileNo, callback){
	var file = fileInput.files[fileNo];
	var textType = /text.*/;

	if (file.type.match(textType)) {
		var reader = new FileReader();

		reader.onload = function(e) {
            callback(reader.result);
		}

		reader.readAsText(file);
	} else {
		alert("File not supported!");
	}
}
