// Product pricing hide / show
$('[id^="product"]').on('show.bs.collapse', function(){
    var clicked = $("[data-target='#" + $(this).attr('id') + "']");
    clicked.removeClass('glyphicon-triangle-bottom');
    clicked.addClass('glyphicon-triangle-top');
}).on('hide.bs.collapse', function(){
    var clicked = $("[data-target='#" + $(this).attr('id') + "']");
    clicked.removeClass('glyphicon-triangle-top');
    clicked.addClass('glyphicon-triangle-bottom');
});
